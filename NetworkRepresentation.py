__author__ = 'jdwinkler'

from collections import defaultdict
import networkx
import numpy
import DatabaseUtils

#design benefit: fold-increase associated with a given design.
#mutations: mutations associated with each gene in a design (add to product dicts)
def generate_assocation_network(design_dicts):

    if(len(design_dicts) == 0):
        return

    edge_weights = defaultdict(int)
    edge_set = set()
    node_mutations = defaultdict(set)
    node_frequency = defaultdict(int)

    #number of mutations is small, no need for faster iteration

    for (mutation_dict, doi, mutant_id) in design_dicts:

        for source in mutation_dict:

            node_frequency[source] = node_frequency[source] + 1

            for sink in mutation_dict:

                if(source == sink):
                    continue

                for tup in mutation_dict[source]:
                    #tup[0] is mutation list, tup[1] is source organism
                    node_mutations[source].add(tup[0])

                edge_weights[(source, sink)] = edge_weights[(source, sink)] + 1
                edge_set.add((source, sink))

    G = networkx.Graph(number_of_designs=len(design_dicts))

    for edge in edge_set:
        G.add_edge(edge[0], edge[1], weight = edge_weights[edge])

    networkx.set_node_attributes(G, 'mutations', dict(node_mutations))
    #networkx.set_node_attributes(G, 'frequency', node_frequency)

    return G, node_frequency

#using the network of genetic modifications + clustering
#want to identify the "core" strain (or strains) that characterize common chassis strains
#inputs:

#g is an undirected network where (u, v) are genes found in the same chassis, and w(u,v) is the count at which these two nodes are associated with one another

#design count is the number of designs used to generate cat_network; used for normalization.

#percentile: frequency at which a pair of genes occur together before they are considered "important", can be [0,1] or [0,100]. default is P = 99% to include only really
#frequent gene pairs.

#nclusters converts node to clusters (opposite of cluster_sets).

#cluster_sets is a  map of cluster id -> node set, used to check automatically if genes are associated without additional heureristics; uses class variable CLUSTERING_METHOD

#returns: set of genes the are highly likely to be found in any design (P > percentile)
#set of possibly associated genes from clustering
def identify_graph_chassis(G, ncluster, number_of_designs, percentile = 0.99):

    if(percentile <= 1.0):
        percentile = int(percentile * 100.0)

    normalized_edge_weights = []

    seen = set()

    #generate set of normalized weights from G
    for edge in G.edges():

        if((edge[0],edge[1]) in seen or (edge[1],edge[0]) in seen):
            continue
        else:
            seen.add((edge[0],edge[1]))

        weight = G[edge[0]][edge[1]]['weight']

        normalized_edge_weights.append((edge[0], edge[1], float(weight)/float(number_of_designs)))

    #calculate the normalized weight that is above percentile

    threshold_weight = numpy.percentile([item[2] for item in normalized_edge_weights], percentile)

    #filter stored edges to identify the ones exceeding the designated percentile
    core_edges = filter(lambda x: x[2] >= threshold_weight, normalized_edge_weights)

    #analyze clustering to see how these edges fall within/between them

    nodes_by_cluster = defaultdict(set)

    potential_chassis_designs = []

    for edge in core_edges:

        nodes_by_cluster[ncluster[edge[0]]].add(edge[0])
        nodes_by_cluster[ncluster[edge[1]]].add(edge[1])

    #hypothesis is that represented clusters are each a design, or at least a reasonable grouping
    for cluster in nodes_by_cluster:

        potential_chassis_designs.append(nodes_by_cluster[cluster])


    #build mutational spectrum for each altered gene so we can report that in the text

    '''
    G = networkx.Graph()

    for e in core_edges:

        u = e[0]
        v = e[1]

        G.add_edge(u, v, {'weight': e[2]})
    '''
    return potential_chassis_designs