import DatabaseUtils
import csv
from regulation_GE import summary_DE
from regulation_GE import summarize_regulators
from regulation_GE import pairwise_ge
import matplotlib

__author__ = 'keerickson'

papers = DatabaseUtils.getDatabase()

output_file = set()

datapath = "data_all_1.27.17.txt"

i = 0  # for mutant num tracking
j = 0  # for gechange tracking
for paper in papers:
    for mutant in paper.mutants:
        if mutant.NumberofGEChanges > 0:
            i += 1  # mutant num tracking
            # output = (str(i) + '\t'+ str(mutant.NumberofGEChanges) + '\n')
            # output_file.add(output)
            for gechange in mutant.gechanges:
                j += 1  # gechange num tracking
                # if gechange.gechange == 'overexpressed':
                #    oe_count += 1
            #   output = (str(i) + '\t' + str(oe_count) + '\n')
                output = (str(i) + '\t' + str(j) + '\t' + gechange.name + '\t' + gechange.gechange + '\t' + str(gechange.foldchange) + '\t'+ str(gechange.exposure) + '\t' + str(gechange.stress_amount) + '\t' + mutant.tolerance[0] + '\t' + str(paper.year) + '\t' + paper.title + '\t' + mutant.strain + '\t' + gechange.gemethod + '\t' + gechange.growthphase+ '\n')
                output_file.add(output)

fhandle = open(datapath,'w')

for line in output_file:
    fhandle.write(line)
fhandle.close()

summary_DE(datapath)  # will return summary info

with open(datapath) as f:
    reader = csv.reader(f, delimiter="\t")
    data = list(reader)

#reg_df = summarize_regulators(data,'Interactions_TF_genes_stndrd.txt')

count = pairwise_ge(data)

