__author__ = 'jdwinkler'

import psycopg2
import psycopg2.extras
import DatabaseUtils
from Standardization import Standard
import os

def search(cur, table, field, name):

    if(name == None):
        return ([],False)

    basicQuery = 'SELECT * from ' + table + ' where UPPER(' + field + ') = UPPER(%s) or UPPER(unique_id) = upper(%s) or upper(%s) = ANY (synonyms) or upper(accession_1) = upper(%s)'
    variables = (name,name, name, name)

    cur.execute(basicQuery, variables)

    records = cur.fetchall()
    if(records != None and len(records) > 0):
        return (records, True)
    else:
        #print englishName
        return ([],False)

def tolerance_phenotype_extractor(papers):

    output = set()

    for paper in papers:

        for mutant in paper.mutants:

            for phenotype in mutant.tolerance:
                output.add(phenotype.replace('_sensitive','').upper())
            for phenotype in mutant.antagonistic_phenotypes:
                output.add(phenotype.upper())

    fhandle = open ('Compound Information.txt','w')

    fhandle.write('Phenotype\n')

    for line in output:
        fhandle.write(line + '\n')

    fhandle.close()

def gene_matcher(papers):

    try:
        connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
    except:
        print "I am unable to connect to the database"
        raise

    cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    found = set()
    not_found = set()

    output_file = [('Resistome Name','Species','Standardized Name')]

    tables = ['ecgenes','ecwgenes','genes']

    found_count = 0
    not_found_count = 0

    for paper in papers:

        for mutant in paper.mutants:

            for mutation in mutant.mutations:

                name = mutation.name

                if(len(name) == 0):
                    raise AssertionError('Zero length name in %s paper record' % paper.title)

                if(name in found):
                    found_count+=1
                    continue
                elif(name in not_found):
                    not_found_count+=1
                    continue

                located = False

                for table in tables:

                    (records, table) = search(cur,table,'common_name',name)

                    if(len(records) > 0):
                        found_count+=1
                        found.add(name)

                        output_name = records[0]['accession_1']

                        if(output_name == None):
                            output_name = records[0]['unique_id']

                        output_file.append((name, mutation.source, output_name))
                        located = True
                        break

                if(not located):
                    not_found_count +=1
                    not_found.add(name)
                    output_file.append((name, mutation.source, name))

    fhandle = open ('Gene Name Mapping.txt','w')

    for line in output_file:
        fhandle.write('\t'.join(line) + '\n')

    fhandle.close()

    print 'Successes: %f ' % (float(found_count) / (float(found_count) + float(not_found_count)))

def gene_matcher_list(list_of_genes):

    try:
        connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
    except:
        print "I am unable to connect to the database"
        raise

    cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    found = set()
    not_found = set()

    output_file = [('Resistome Name','Species','Standardized Name')]

    tables = ['ecgenes','ecwgenes','genes']

    found_count = 0
    not_found_count = 0

    for name in list_of_genes:

        if(len(name) == 0):
            raise AssertionError('Zero length name in %s paper record' % paper.title)

        if(name in found):
            found_count+=1
            continue
        elif(name in not_found):
            not_found_count+=1
            continue

        located = False

        for table in tables:

            (records, table) = search(cur,table,'common_name',name)

            if(len(records) > 0):
                found_count+=1
                found.add(name)

                output_name = records[0]['accession_1']

                if(output_name == None):
                    output_name = records[0]['unique_id']

                output_file.append((name, 'Escherichia coli', output_name))
                located = True
                break

        if(not located):
            not_found_count +=1
            not_found.add(name)
            output_file.append((name, 'not found', name))

    fhandle = open ('Gene Name Mapping Additions.txt','w')

    for line in output_file:
        fhandle.write('\t'.join(line) + '\n')

    fhandle.close()

    print 'Successes: %f ' % (float(found_count) / (float(found_count) + float(not_found_count)))

def ontology_updater(papers, phenotype_standard):

    stress_ontology, classifier = DatabaseUtils.load_tolerance_ontology()

    missing = set()

    for paper in papers:

        for mutant in paper.mutants:

            phenotypes = []
            phenotypes.extend(mutant.resistant_phenotypes)
            phenotypes.extend(mutant.sensitive_phenotypes)

            for p in mutant.tolerance:
                if(p not in stress_ontology):
                    print p
                    missing.add(p)

    fhandle = open('Ontology Updates (kinda).txt','w')

    for line in missing:
        fhandle.write(line + '\n')
    fhandle.close()

if(__name__ == '__main__'):

    standard_obj = Standard(os.path.join(DatabaseUtils.INPUT_DIR,'Gene Name Mapping.txt'))

    phenotye_std = Standard(os.path.join(DatabaseUtils.INPUT_DIR, 'Compound Information.txt'))

    papers = DatabaseUtils.getDatabase(standard_obj, phenotye_std)

    #tolerance_phenotype_extractor(papers)
    #gene_matcher(papers)

    #ontology_updater(papers, phenotye_std)

    '''

    output = []

    for paper in papers:
        for mutant in paper.mutants:

            output.extend(mutant.ancillary_genes)

    gene_matcher_list(set(output))
    '''

    fhandle = open('Unconverted Essential Genes.txt','rU')
    lines = fhandle.readlines()
    fhandle.close()

    gene_matcher_list(set([x.strip() for x in lines]))




#build product ontology
#build gene ontology
#implement recommendation system to identify genes that are related to data in laser
#feature grouping to compare modified systems based on gene ontology data
#improve curation and add genes
#implement format changes already done for laser 2015