import os
from MetEngDatabase import MetEngDatabase
from MetEngDatabase import Paper
from collections import defaultdict

INPUT_DIR = os.getcwd() + os.sep + 'inputs'+ os.sep
OUTPUT_DIR = os.getcwd() + os.sep + 'output'+ os.sep
DATABASE_DIR = os.getcwd() + os.sep + "database_store" + os.sep
ERROR_DIR = os.getcwd() + os.sep + 'errors'+ os.sep

#define universal constants relating to mutation classification
OE_mutations     = set(['amplified','oe','plasmid','rbs_tuned'])
REP_mutations    = set(['rep','antisense'])
DEL_mutations    = set(['del','frameshift','is_insertion','indel'])
RANDOM_mutations = set(['aa_snps','nuc_snps'])
ADD_mutations    = set(['sensor','regulated'])
NO_mutations     = 'none'

def networkx_to_igraph(networkx_graph, directed = True):

    import igraph
    import networkx

    nodes = networkx_graph.nodes()
    edges = networkx_graph.edges()

    nodes_to_int = {}
    int_to_nodes = {}

    for i in range(0,len(nodes)):
        nodes_to_int[nodes[i]] = i
        int_to_nodes[i] = nodes[i]

    #convert edges

    converted_edges = []

    converted_weights = []

    for edge in edges:
        node1 = nodes_to_int[edge[0]]
        node2 = nodes_to_int[edge[1]]


        if('weight' in networkx_graph[edge[0]][edge[1]]):
            converted_weights.append(networkx_graph[edge[0]][edge[1]]['weight'])
        converted_edges.append((node1,node2))

    #build igraph,
    #compute betweenness,
    #converte btw scores back to networkx node names

    if(directed == False):
        directed = None

    if(converted_weights != []):

        G = igraph.Graph(n = len(nodes), edges = converted_edges, directed = directed, edge_attrs={'weight':converted_weights})

    else:

        G = igraph.Graph(n = len(nodes), edges = converted_edges, directed = directed)

    return G, int_to_nodes, converted_weights

#clusters graphs into communities using leading eigenvector by MEJ Newmann method
#citation: Finding community structure in networks using the eigenvectors of matrices
#I tried other methods, but they just make one super cluster with >1000 others with very few nodes in them.
#betweenness: too computationally expensive
#multilevel: few clusters with many nodes
#spinglass: doesn't work (bug or user error?)
#infomap: few clusters with many nodes
#MCL: haven't tried, need to build a wrapper for calling from python, build the software for windows

def compute_igraph_clustering(networkx_graph, method = None):

    G, int_to_nodes, converted_weights = networkx_to_igraph(networkx_graph, directed = True)

    if(converted_weights == []):
        converted_weights = None

    if(method == 'leading_eigenvector'):
        clusters = G.community_leading_eigenvector(weights=converted_weights)#.as_clustering()
    elif(method == 'infomap'):
        clusters = G.community_infomap(edge_weights=converted_weights)
    elif(method == 'edge_betweenness'):
        clusters = G.community_edge_betweenness(weights=converted_weights).as_clustering()
    elif(method == 'multilevel'):
        clusters = G.as_undirected().community_multilevel(weights=converted_weights)
    elif(method == 'walktrap'):
        clusters = G.community_walktrap(weights=converted_weights).as_clustering()
    elif(method == 'fastgreedy'):
        clusters = G.as_undirected().community_fastgreedy(weights=converted_weights).as_clustering()
    elif(method == 'label_propagation'):
        clusters = G.as_undirected().community_label_propagation(weights=converted_weights)
    else:
        #break if invalid clustering selection selected
        raise KeyError('Unrecognized clustering method provided: %s' % method)

    #clusters = dendrogram.as_clustering()

    membership = clusters.membership

    nodes_to_clusters = {}

    cluster_membership = defaultdict(list)

    for i in range(0, len(membership)):

        node_id = int_to_nodes[i]
        nodes_to_clusters[node_id] = membership[i]

        cluster_membership[membership[i]].append(node_id)
    
    return nodes_to_clusters, cluster_membership

#reconstructs the passed info to an igraph
def compute_igraph_evcentrality(networkx_graph, directed = True):

    import networkx
    import igraph

    G, int_to_nodes, converted_weights = networkx_to_igraph(networkx_graph, directed = directed)
    
    #already normalized

    if(converted_weights == []):
        converted_weights = None
    
    ec = G.eigenvector_centrality(weights = converted_weights)

    ec_dict = {}

    for i in range(0, len(ec)):
        ec_dict[int_to_nodes[i]] = ec[i]
        
    return ec_dict


#reconstructs the passed info to an igraph
def compute_igraph_betweenness(networkx_graph, directed = True):

    import networkx
    import igraph

    G, int_to_nodes, converted_weights = networkx_to_igraph(networkx_graph, directed = directed)

    if(converted_weights == []):
        converted_weights = None
    
    bc = G.betweenness(weights = converted_weights)

    #normalizing value
    max_value = max(bc)

    bc_dict = {}
    #0-1.0 scale for all networks
    for i in range(0, len(bc)):
        bc_dict[int_to_nodes[i]] = float(bc[i])/float(max_value)
        
    return bc_dict

def compute_frequency(default_dict):

    d_sum = 0

    fdict = {}

    for key in default_dict:
        d_sum = d_sum + default_dict[key]

    for key in default_dict:
        fdict[key] = float(default_dict[key])/float(d_sum)

    return fdict

def normalize_dict(tfdict, rfdict):

    output_dict = defaultdict(float)

    for key in tfdict:

        if(key in rfdict and rfdict[key] != 0):
            output_dict[key] = tfdict[key]/rfdict[key]
        else:
            print 'Invalid or missing key from reference dictionary'
            raise ValueError

    return output_dict
        
def writefile(header, outputList, delim,  outputName):

    data = [header]
    
    for item in outputList:
        data.append(item)
        
    fhandle = open(outputName,'w')

    for line in data:
        line = [str(x) for x in line]
        fhandle.write(delim.join(line) + "\n")
    fhandle.close()

def load_mutation_abbrevs():

    mut_mapper = {}
    mut_mapper['oe'.upper()] = 'OE'
    mut_mapper['plasmid'.upper()] = 'PLA'
    mut_mapper['del'.upper()] = 'DEL'
    mut_mapper['integrated'.upper()] = 'INT'
    mut_mapper['terminated'.upper()] = 'TER'
    mut_mapper['aa_snps'.upper()] = 'AA*'
    mut_mapper['codonoptimized'.upper()] = 'C-OPT'
    mut_mapper['amplified'.upper()] = 'AMP'
    mut_mapper['truncated'.upper()] = 'TRC'
    mut_mapper['nuc_snps'.upper()] = 'SNP'
    mut_mapper['mutated'.upper()] = 'UNK'
    mut_mapper['rep'] = 'REP'
    mut_mapper['is_insertion'] = 'TN'
    mut_mapper['intergenic'] = 'BTW'
    mut_mapper['frameshift'] = 'FRAME'
    mut_mapper['large_deletion'] = 'LDEL'
    mut_mapper['large_amplification'] = 'LAMP'
    mut_mapper['duplication'] = 'DUPE'
    mut_mapper['compartmentalization'.upper()] = 'COMP'
    mut_mapper['con'.upper()] = 'CON'
    mut_mapper['scaffold_binder'.upper()] = 'SCFB'
    mut_mapper['protein_fusion'.upper()] = 'FUSE'
    mut_mapper['indel'.upper()] = 'INDEL'
    mut_mapper['antisense'.upper()] = 'ASEN'
    mut_mapper['rbs_tuned'.upper()] = 'RBS-T'
    mut_mapper['regulated'.upper()] = 'REG'
    mut_mapper['Other'] = 'OTH'
    mut_mapper['none'] = 'NONE'

    return mut_mapper

def load_tolerance_abbrevs():

    umapper = {}
    umapper['solvents_biofuels'] = 'SB'
    umapper['antichemo'] = 'AAC'
    umapper['osmotic_stress'] = 'OSM'
    umapper['organic_acid (neutral)'] = 'OA'
    umapper['furans'] = 'FUR'
    umapper['high_ph'] = 'BASE'
    umapper['low_ph'] = 'ACID'
    umapper['oxidative'] = 'OXI'
    umapper['high_temperature'] = 'HOT'
    umapper['low_temperature'] = 'COLD'
    umapper['nutrient_limitation'] = 'NTL'
    umapper['other'] = 'OTH'
    umapper['general_growth'] = 'GROW'
    umapper['aa_antimetabolite'] = 'AAAM'
    umapper['general_antimetabolite'] = 'ANTIM'
    umapper['radiation'] = 'RAD!'
    umapper['metal_ions'] = 'METI'
    umapper['detergents'] = 'DETR'
    umapper['quaternary_ammonia'] = 'NH4+'
    umapper['mutagens'] = 'MUTA'
    umapper['high_gravity'] = 'HIGHG'

    for key in umapper.keys():
        umapper[key.upper()] = umapper[key]


    return umapper

def load_use_abbrevs():

    umapper = {}
    umapper['BIOFUELS'] = 'BF'
    umapper['BIOMASS'] = 'BIOM'
    umapper['BIOREMEDIATION'] = 'REMD'
    umapper['COSMETICS'] = 'COSM'
    umapper['FOOD ADDITIVES'] = 'FOOD'
    umapper['ORGANIC ACIDS'] = 'OACID'
    umapper['pharmaceuticals'.upper()] = 'PHARM'
    umapper['POLYMERS'] = 'POLY'
    umapper['FEEDSTOCK'] = 'FSTOCK'
    return umapper

def load_category_abbrevs():

    cmapper = {}
    cmapper['ALCOHOLS'] = 'ALC'
    cmapper['AMINO ACIDS'] = 'AA'
    cmapper['BIOMASS'] = 'BIOM'
    cmapper['BIOPOLYMER'] = 'POLY'
    cmapper['CHALCONE'] = 'CHAL'
    cmapper['DIAMINE'] = 'DIA'
    cmapper['DIOLS'] = 'DIOLS'
    cmapper['DNA'] = 'DNA'
    cmapper['ethylene glycol'.upper()] = 'ETGL'
    cmapper['FATTY ACIDS'] = 'FA'
    cmapper['flavanoids'.upper()] = 'FLAV'
    cmapper['HEAVY METALS'] = 'HEAV'
    cmapper['HYDROCARBON'] = 'HYC'
    cmapper['HYDROGEN'] = 'H2'
    cmapper['metabolic intermediates'.upper()] = 'META'
    cmapper['organic acids'.upper()] = 'OACID'
    cmapper['PHENOLICS'] = 'PHEN'
    cmapper['protein'.upper()] = 'PROT'
    cmapper['SOLVENTS'] = 'SOLV'
    cmapper['sugar alcohol'.upper()] = 'SUGA'
    cmapper['TERPENOIDS'] = 'TERP'
    cmapper['VITAMINS'] = 'VITA'
    return cmapper

def load_journal_abbrevs():
    
    journal_mapper = {}
    journal_mapper['Metabolic Engineering'.upper()] = 'ME'
    journal_mapper['Applied and Environmental Microbiology'.upper()] = 'AEM'
    journal_mapper['Microbial Cell Factories'.upper()] = 'MCF'
    journal_mapper['Biotechnology and Bioengineering'.upper()] = 'BB'
    journal_mapper['Applied Microbiology and Biotechnology'.upper()] = 'AMB'
    journal_mapper['PNAS'] = 'PNAS'
    journal_mapper['Nature Biotechnology'.upper()] = 'NBT'
    journal_mapper['Journal of Industrial Microbiology and Biotechnology'.upper()] = 'JIMB'
    journal_mapper['ACS Synthetic Biology'.upper()] = 'ACSB'
    journal_mapper['APPLIED BIOTECHNOLOGY AND MICROBIOLOGY'] = 'ABM'
    journal_mapper['FEMS YEAST RESEARCH'] = 'FEMY'
    journal_mapper['JOURNAL OF BIOTECHNOLOGY'] = 'JBIOT'
    journal_mapper['NATURE'] = 'NAT'
    journal_mapper['NATURE CHEMICAL BIOLOGY'] = 'NATCB'
    journal_mapper['PLOS GENETICS'] = 'PLGE'
    journal_mapper['BIOTECHNOLOGY FOR BIOFUELS'] = 'BFBF'
    journal_mapper['NATURE COMMUNICATIONS'] = 'NCOMM'
    journal_mapper['WILEY INTERSCIENCE'] = 'WINT'
    journal_mapper['BIOSCIENCE, BIOTECHNOLOGY, AND BIOCHEMISTRY'] = 'B3'
    journal_mapper['BIOTECHNOLOGY PROGRESS'] = 'BPROG'
    journal_mapper['JOURNAL OF APPLIED MICROBIOLOGY'] = 'JAM'
    journal_mapper['NUCLEIC ACIDS RESEARCH'] = 'NAR'
    journal_mapper['ORGANIC AND BIOMOLECULAR CHEMISTRY'] = 'OBC'

    journal_mapper['Other'] = 'OTH'

    return journal_mapper


def load_tolerance_ontology():

    fhandle = open(os.path.join(INPUT_DIR,'Stress Classifications.txt'),'rU')
    lines = fhandle.readlines()
    fhandle.close()

    tags = {}
    ontology = {}

    for line in lines[1:]:

        tokens = line.strip().replace('\"','').split('\t')

        stressor = tokens[0].upper().replace('_sensitive'.upper(),'')
        tagged_info = set([x.upper() for x in tokens[1].split(',')])
        general_category = set([x.upper() for x in tokens[2].split(',')])
        root_category    = tokens[3]

        ontology[stressor.upper()] = (root_category,general_category)
        tags[stressor.upper()] = tagged_info

        if('_sensitive'.upper() not in stressor):
            tags[(stressor+'_sensitive').upper()] = tagged_info
            ontology[(stressor+'_sensitive').upper()] = (root_category, general_category)

    return tags, ontology

def load_essential_genes(standard_obj, species):

    if(species.upper() != 'Escherichia coli'.upper()):
        raise AssertionError('You will need to find the essential genes table for your species (or checking E. coli spelliing: %s' % species)

    fhandle = open(os.path.join(INPUT_DIR, 'essential_genes_ecoli.txt'), 'rU')

    essential_genes = []

    fhandle.readline()

    for line in fhandle:

        tokens = line.strip().split('\t')

        essential_genes.append(standard_obj.convert(tokens[0], species))

    fhandle.close()

    return essential_genes

def load_seed_subsystems(standard_obj, species):

    if(species.upper() != 'Escherichia coli'.upper()):
        raise AssertionError('You will need to go get seed table for the organism you requested (or check Escherichia coli spelling: %s' % species)

    fhandle = open(os.path.join(INPUT_DIR,'Converted SEED Subsystem Labels.txt'),'rU')

    header = fhandle.readline().strip().split('\t')

    c = {}
    for i,token in zip(range(0,len(header)),header):
        c[token] = i

    output_dict = defaultdict(set)

    for line in fhandle:

        tokens = line.strip().split('\t')

        subsystems = [x.strip() for x in tokens[c['Subsystem']].split(';')]
        bnumber    = tokens[c['bnumber']]

        for x in subsystems:

            if('- none -' not in x):
                output_dict[standard_obj.convert(bnumber.upper(),species)].add(x)

    return output_dict

def filter_database(papers, field_to_filter, include_tags):

    print 'not implemented yet'

def getDatabase(standard = None, phenotype_standard = None, getParser = False, years = ('2017_ge','2016_v2_compatible')): #change to '2017_ge' for whole ge database

    from Standardization import Standard

    inputTerms = os.path.join(INPUT_DIR,'InputFields_typed.txt')

    annotation_terms = open(os.path.join(INPUT_DIR, 'Annotation Grammar_typed.txt'),'rU')

    if(standard == None):

        standard= Standard(os.path.join(INPUT_DIR,'Gene Name Mapping.txt'))

    if(phenotype_standard == None):

        phenotype_standard = Standard(os.path.join(INPUT_DIR, 'Compound Information.txt'))

    annotation_lines = annotation_terms.readlines()
    annotation_terms.close()

    parserRules     = {}

    headers = annotation_lines[0].strip().split('\t')
    c = {}

    for i, token in zip(range(0, len(headers)), headers):
        c[token] = i

    for line in annotation_lines[1:]:

        tokens = line.strip().split('\t')

        mutation_name = tokens[c['Mutation']]
        mutation_type = tokens[c['Type']]
        category      = tokens[c['Category']]
        is_list       = tokens[c['List']] == '1'
        is_tuple      = tokens[c['Tuple']] == '1'
        delimiter     = tokens[c['Delimiter']]

        if(delimiter.upper() == 'NONE'):
            delimiter = ''

        parsing_dict = {'type':mutation_type,'is_list':is_list,'is_tuple':is_tuple,'list_key':delimiter,'category':category}

        parserRules[mutation_name] = parsing_dict

    fhandle = open(inputTerms,'rU')
    inputs = fhandle.readlines()
    fhandle.close()

    dataTerms = []
    muttTerms = []
    geneTerms = []
    geTerms = []

    termCategoryMap = {}
    termTypeMap     = {}
    termRequiredMap = {}
    termEntryMap    = {}
    termMultipleMap = {}

    headers = inputs[0].strip().split('\t')
    c = {}

    for i, token in zip(range(0, len(headers)), headers):
        c[token] = i

    for line in inputs[1:]:

            line = line.translate(None,'\n\"')
            tokens = line.split("\t")
            #from InputFields_typed.txt
            category  = tokens[c['Category']]
            entryName = tokens[c['Internal Name']]
            display   = tokens[c['Displayed Name']]
            inputType = tokens[c['Type']]
            inputRequired = tokens[c['Required?']]
            multipleValues= tokens[c['Can select multiple?']]
            inputEntry    = tokens[c['Key for dropdown entries']]
            is_list   = tokens[c['List?']]
            list_delimiter= tokens[c['Delimiter']]

            if(category == 'paper'):
                    dataTerms.append(entryName)

            if(category == 'mutant'):
                    muttTerms.append(entryName)

            if(category == 'gene'):
                    geneTerms.append(entryName)

            if(category == 'hidden'):
                    dataTerms.append(entryName)

            if(category == 'gechange'):
                    geTerms.append(entryName)

            parsing_dict = {}
            parsing_dict['type'] = inputType
            parsing_dict['is_list'] = is_list == '1'
            parsing_dict['list_key'] = list_delimiter
            parsing_dict['is_dropdown'] = inputEntry.upper() != 'NONE'
            parsing_dict['dropdown_key'] = inputEntry
            parsing_dict['multiple'] = multipleValues == '1'
            parsing_dict['category'] = 'not used here'

            termCategoryMap[entryName] = display
            termTypeMap[entryName]     = inputType
            termRequiredMap[entryName] = inputRequired
            termEntryMap[entryName]    = inputEntry
            termMultipleMap[entryName] = multipleValues
            parserRules[entryName] = parsing_dict

    numMK = 'NumberofMutants'
    numGK = 'NumberofMutations'
    numTXN = 'NumberofGEChanges'

    paths = []

    for year in years:

        path = os.path.join(DATABASE_DIR, year)

        record_file_list = os.listdir(path)

        paths.extend([os.path.join(path,x) for x in record_file_list])

    database = MetEngDatabase(paths, numMK, numGK, numTXN, dataTerms, muttTerms, geneTerms, geTerms, parserRules, termTypeMap, [], standard_obj=standard, phenotype_std=phenotype_standard)

    if(getParser):
        return database.records, parserRules
    else:
        return database.records
