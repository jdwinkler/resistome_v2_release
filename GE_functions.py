#Find and visualize information about regulators
from __future__ import division
import csv
import os
import pandas
import scipy
from scipy import stats
import matplotlib.pyplot as plt
import numpy as np
import itertools

INPUT_DIR = os.getcwd() + os.sep + 'inputs'+ os.sep


def summary_DE(datapath):
    with open(datapath) as f:
        reader = csv.reader(f,delimiter="\t")
        data = list(reader)

    # just the list of genes
    genes_all = [row[2]for row in data]
    print 'All DE genes: ', len(genes_all)
    genes_unique = set(genes_all)
    print 'Unique genes: ', len(genes_unique)

    # just the list of phenotypes
    phenotypes = set([row[7]for row in data])
    print 'Number of phenotypes: ', len(phenotypes)

    # number of entries with GE data
    no_entries = max([int(row[0]) for row in data])
    print 'Number of entries with GE data: ', no_entries

    # number of strains
    strains = set([row[10] for row in data])
    print 'Number of strains: ', len(strains)

    # years
    years = [row[8] for row in data]
    print 'Earliest year: ', min(years)
    print 'Latest year: ', max(years)


def summarize_regulators(data,reg_txt):
    reg_file = os.path.join(INPUT_DIR,reg_txt)
    with open(reg_file) as regf:
        reader = csv.reader(regf, delimiter="\t")
        reg_info = list(reader)

    reg_set = list(frozenset([row[0] for row in reg_info]))  # first column has regulators
    tags_set = list(frozenset([row[7] for row in data]))  # all unique phenotypes
    reg_df = pandas.DataFrame(0,index=reg_set, columns=tags_set)
    # make reg df, which has actual counts
    for i in range(0,len(data)):
        de_gene = data[i][2]  # get the gene name
        phenotype = data[i][7]  # phenotype
        for j in range(0,len(reg_info)):
            if de_gene == reg_info[j][1]:  # if DE gene is regulated by this TF/sigma factor
                regulator = reg_info[j][0]
                reg_df[phenotype][regulator] = reg_df[phenotype][regulator] + 1 #update count

    # make expected counts list, has total number of gene regulated for each TF
    expected_count = [0]*len(reg_set)  # make an empty list
    i = 0
    for r in reg_set:  # for that unique regulator
        i += 1
        for j in range(0,len(reg_info)):
            if r == reg_info[j][0]:  # if this is the regulator
                expected_count[i-1] += 1

    # make probability list, has % of genes regulated by each TF
    probability = [x / sum(expected_count) for x in expected_count]  # this will just return all zeros without that import division statement at the top

    # count all DE genes from actual counts
    sums_by_condition = reg_df.sum(axis=0)

    # run binomial test. x = number of de genes for that regulator, n = number of de genes total, p = expected probability
    pval_df = pandas.DataFrame(0, index=reg_set, columns=tags_set, dtype='float')
    for i in range(0, len(reg_df.columns)):  # for each phenotype
        num = sums_by_condition[i]  # number of DE genes in that phenotype
        phenotype = tags_set[i]
        for j in range(0, len(reg_df.index)):  #for each regulator
            regulator = reg_set[j]
            x = reg_df[phenotype][regulator]
            prob = probability[j]
            pval = scipy.stats.binom_test(x, num, prob)
            pval_df[phenotype][regulator] = pval
    reg_df.to_excel('reg_tf_count_output.xlsx', 'Sheet1')
    pval_df.to_excel('reg_tf_pval_output.xlsx', 'Sheet1')

    return reg_df


# count how many times two genes are DE in the same strain
def pairwise_ge(data):

    mutants_all = list(([row[0] for row in data]))  # all mutants
    mutants_set = list(frozenset(row[0] for row in data))  # all mutants (unique)
    genes_set = list(frozenset([row[2] for row in data]))  # all genes (unique)
    genes_all = list([row[2] for row in data])  # all genes

    gene_pairs = list(itertools.combinations(genes_set, 2))
    gene_df = pandas.DataFrame({'gene_pairs': gene_pairs})

    pairwise_count = [0] * len(gene_pairs)  # make an empty list

    for j in range(0, len(mutants_set)):
        focus_mutant = mutants_set[j]
        mutant_gene_list = []  # empty list for each mutant
        for i in range(0,len(mutants_all)):
            if mutants_all[i] == focus_mutant:
                mutant_gene_list.append(genes_all[i])
        for k in range(0,len(gene_pairs)):
            if gene_pairs[k][0] in mutant_gene_list and gene_pairs[k][1] in mutant_gene_list:
                pairwise_count[k] += 1

    gene_df["Count"] = pairwise_count
    gene_df.to_excel('gene_pairs.xlsx', 'Sheet1')

