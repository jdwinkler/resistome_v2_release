__author__ = 'jdwinkler'

import DatabaseUtils
import os
from Standardization import Standard

standard_obj = Standard(os.path.join(DatabaseUtils.INPUT_DIR,'Gene Name Mapping.txt'))

phenotype_obj = Standard(os.path.join(DatabaseUtils.INPUT_DIR, 'Compound Information.txt'))

papers = DatabaseUtils.getDatabase(standard_obj, phenotype_obj)

annotations = set()

for paper in papers:
    for mutant in paper.mutants:
        for mutation in mutant.mutations:
            for change in mutation.changes:
                annotations.add((change,))

fhandle = open ('Annotation Grammar_typed.txt','w')

for line in annotations:
    fhandle.write('\t'.join(line) + '\n')
fhandle.close()

