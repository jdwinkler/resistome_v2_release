from collections import defaultdict
from MetEngDatabase import Paper, Mutant, Mutation
import networkx
import GraphUtils as glib
import DatabaseUtils
import os
import numpy
import scipy
import scipy.stats
import scipy.misc
from decimal import *
import warnings
from Standardization import Standard
import gene_ontology


def generate_ordered_list(value_dict, threshold):

    output_dict = defaultdict(int)

    non_other_keys = []

    for key in value_dict:
        if(value_dict[key] <= threshold):
            output_dict['Other'] = output_dict['Other'] + value_dict[key]
        else:
            output_dict[key] = value_dict[key]
            non_other_keys.append(key)

    output_tuples = []

    for key in non_other_keys:
        output_tuples.append((key, output_dict[key]))

    output_tuples = sorted(output_tuples, reverse=True, key=lambda tup: tup[1])
    output_tuples.append(('Other', output_dict['Other']))

    ordered_keys = [item[0] for item in output_tuples]
    ordered_values =  [item[1] for item in output_tuples]
    
    return output_dict, ordered_keys, ordered_values 

def count_occurence(value, test, papers, parsingMethod, numMK='NumberofMutants',numGK='NumberofMutations'):

    count = 0

    def update_count(count, data):

        if(type(data) != list):
            if(data.upper() == test.upper()):
                count = count + 1
            return count
        else:
            for token in data:
                if(token.upper() == test.upper()):
                    count = count + 1
                    
            return count
        
        warnings.warn('Unknown parsing method (%s) detected-you should update the update_count method.' % parsing_type)

    processed_already = set()

    for paper in papers:

        if(paper.doi in processed_already):
            continue

        processed_already.add(paper.doi)

        if(hasattr(Paper, value)):
            count = update_count(count, getattr(Paper, value).fget(paper))

        if(hasattr(Mutant, value)):
            
            #mutants = int(paper.paperBacking[numMK])

            mutants = paper.mutants

            for mut in mutants:
                count = update_count(count, getattr(Mutant, value).fget(mut))
                
        if(hasattr(Mutation, value)):
            for mutant in paper.mutants:
                for mutation in mutant.mutations:
                    count = update_count(count,  getattr(Mutation, value).fget(mutation))
                    
    return count
        
def distribution(value, threshold, papers, parsingMethod, numMK='NumberofMutants',numGK='NumberofMutations', mapper = None):

    def update_dict(value_dict, data):

        if(type(data) != list):

            if(mapper != None and data.upper() in mapper):
                value_dict[mapper[data.upper()]] = value_dict[mapper[data.upper()]]+1
            else:
                value_dict[data.upper()] = value_dict[data.upper()]+1
            return value_dict
        else:
            for token in data:
                if(mapper != None and token.upper() in mapper):
                    value_dict[mapper[token.upper()]] = value_dict[mapper[token.upper()]] + 1
                else:
                    value_dict[token.upper()] = value_dict[token.upper()] + 1

            return value_dict

        warnings.warn('Unknown parsing method detected-you should update the update_dict method.')

    value_dict = defaultdict(int)

    print value

    for paper in papers:

        if(hasattr(Paper, value)):
            value_dict = update_dict(value_dict, getattr(Paper, value).fget(paper))

        if(hasattr(Mutant, value)):
            mutants = paper.mutants
            for mut in mutants:
                value_dict = update_dict(value_dict, getattr(Mutant, value).fget(mut))

        if(hasattr(Mutation, value)):
            for mutant in paper.mutants:
                for mutation in mutant.mutations:
                    value_dict = update_dict(value_dict,  getattr(Mutation, value).fget(mutation))

    return generate_ordered_list(value_dict, threshold)

def interaction_matrix(source_data, rows, genome_size = 4377, significance_threshold=0.0001):

    '''

    statistical test for significant overlaps: http://nemates.org/MA/progs/representation.stats.html

    Two groups of genes are compared and found to have x genes in common. A representation factor and the probability of of finding an overlap of x genes are calculated.
    Representation factor
    The representation factor is the number of overlaping genes divided by the expected number of overlaping genes drawn from two independent groups.
    A representation factor > 1 indicates more overlap than expected of two independent groups, a representation factor < 1 indicates less overlap than expected, and a representation factor of 1 indicates that the two groups by the number of genes expected for independent groups of genes.

    x = # of genes in common between two groups.
    n = # of genes in group 1.
    D = # of genes in group 2.
    N = total genes, in this case the 17611 genes with good spots on the Kim lab full genome chips.
    C(a,b) is the number of combinations of a things taken b at a time.
    The representation factor = x / expected # of genes.
    Expected # of genes = (n * D) / N

    Probability
    Exact hypergeometric probability

    The probability of finding x overlapping genes can be calculated using the hypergeometric probability formula:
    C(D, x) * C(N-D, n-x) / C(N,n)

    If x is less than the expected number of overlapping genes, the probability of finding x or fewer genes is:

    Prob = sum (i=0 to i=x) [ C(D, i) * C(N-D, n-i) / C(N,n) ]

    If x is greater than the expected number of overlapping genes, the probability of finding x or more genes is:

    Prob = 1- sum (i=0 to i=(x-1)) [ C(D, i) * C(N-D, n-i) / C(N,n) ]

    C(a,b) is calculated using:

    C(a,b) = a! / ((a - b)! * b!)
    C(a,b) = exp(gammln(a + 1)-gammln(1+(a - b))-gammln(b + 1))
    gamma(a + 1) = a!.
    and gammln(a + 1) is an approximation of a! calculated using code from Chapter 6.1 of Numerical Recipes in C: The Art of Scientific Computing (ISBN 0-521-43108-5)

    '''

    numpy.seterr(all='raise')

    #C_hyper = lambda a,b: scipy.special.gammaln(a + 1)-scipy.special.gammaln(1+(a - b))-scipy.special.gammaln(b + 1)

    associative_matrix = defaultdict(dict)

    significant_tuples = []

    for condition_x in rows:

        x_set = set(source_data[condition_x])
        x_len = len(x_set)

        for condition_y in rows:

            if(condition_x == condition_y):
                associative_matrix[condition_x][condition_y] = 0.0
                continue

            if(condition_x in source_data and condition_y in source_data):

                if(condition_y in associative_matrix[condition_x]):
                    #already done
                    continue

                #expected number of overlapping genes

                y_set = set(source_data[condition_y])
                y_len = len(set(source_data[condition_y]))
                overlap = len(x_set & y_set)
                expected_number = float(x_len) * float(y_len) / float(genome_size)

                value3 = scipy.misc.comb(genome_size,x_len, exact=True, repetition=True)

                #print x_len, y_len, overlap, 'initial input'

                if(expected_number == 0):
                    associative_matrix[condition_x][condition_y] = 0
                    associative_matrix[condition_y][condition_x] = 0
                    continue

                if(overlap < expected_number):
                    probability = Decimal(0)
                    #0 to overlap inclusive
                    for i in range(0, overlap+1):
                        value1 = scipy.misc.comb(y_len, i, exact=True, repetition=True)
                        value2 = scipy.misc.comb(genome_size-y_len, x_len-i, exact=True, repetition=True)
                        probability += Decimal(value1) * Decimal(value2) / Decimal(value3)
                else:
                    #overlap - 1
                    probability = Decimal(1)
                    for i in range(0, overlap):
                        value1 = scipy.misc.comb(y_len, i, exact=True, repetition=True)
                        value2 = scipy.misc.comb(genome_size-y_len,x_len-i, exact=True, repetition=True)
                        probability -= Decimal(value1) * Decimal(value2) / Decimal(value3)

                #print probability

                if(probability < 0 or probability > 1.0):
                    print condition_x,condition_y, probability, 'weird value? possible numeric error'
                    probability = abs(probability)

                associative_matrix[condition_x][condition_y] = abs(numpy.log10(float(probability)))
                associative_matrix[condition_y][condition_x] = abs(numpy.log10(float(probability)))

                if(probability < significance_threshold):
                    significant_tuples.append((condition_x, condition_y, probability))

            #print condition_x, condition_y, normed_counts

    #now pruned.
    rows = associative_matrix.keys()

    data = []
    for condition_x in rows:

        columns = []
        #symmetrical matrix
        for condition_y in rows:

            if(condition_y in associative_matrix[condition_x]):
                columns.append(associative_matrix[condition_x][condition_y])
            else:
                columns.append(0)

        data.append(columns)

    matrix = numpy.matrix(data)

    #print 'Remember the minimum p-value is 10^-15!'

    return matrix,rows, significant_tuples


def construct_bipartite_network(resistant_dict, sensitive_dict):

    edges = []

    seen_before = set()

    node_set1 = set()
    node_set2 = set()

    for key in resistant_dict:

        node_set1.add(key)

        for y in resistant_dict[key]:

            if(y in seen_before):
                continue

            seen_before.add(y)
            node_set2.add(y)

            edges.append((key, y, {'weight': resistant_dict[key].count(y)}))

    seen_before = set()

    for key in sensitive_dict:
        node_set1.add(key)

        for y in sensitive_dict[key]:

            if(y in seen_before):
                continue

            node_set2.add(y)
            seen_before.add(y)

            edges.append((key, y, {'weight': -sensitive_dict[key].count(y)}))

    G = networkx.Graph()

    G.add_edges_from(edges)

    return G, node_set1, node_set2

def bipartite_analysis(resistant_tag_overlap):

    pruned_network = {}
    counter = defaultdict(int)

    for key in resistant_tag_overlap:
        for gene in set(resistant_tag_overlap[key]):
            counter[gene] +=1

    isolates = [x for x in counter if counter[x] == 1]

    for phenotype in resistant_tag_overlap:
        pruned_network[phenotype] = resistant_tag_overlap[phenotype]

        for iso in isolates:
            if(iso in pruned_network[phenotype]):
                pruned_network[phenotype].remove(iso)

    network, phenotypes, genes = construct_bipartite_network(pruned_network, {})

    node_to_clusters, cluster_membership = DatabaseUtils.compute_igraph_clustering(network,method='fastgreedy')

    color_function = glib.get_cmap(len(cluster_membership.keys())*2+1)

    for node in network.nodes():

        if(node in phenotypes):
            network.node[node]['color'] = color_function(len(cluster_membership.keys())*2+1)
            network.node[node]['size'] = 150
        if(node in genes):
            network.node[node]['color'] = color_function(node_to_clusters[node]*2)


    glib.plot_network(network, 'Test Bipartite', os.path.join(DatabaseUtils.OUTPUT_DIR,'Tolerance Bipartite Network.pdf'))


#generate distribution of modified genes figure
#generate heatmap of genes commonly associated with one another

def association_figure_categories(papers, target_categories, genome_size, threshold = 0, output_prefix = ''):

    #todo: make gene-gene overlap figure.
    processed = set()

    if(target_categories == None):
        target_categories = set()
    else:
        target_categories = {x.upper() for x in target_categories}

    counting = defaultdict(int)

    geneCount = 0
    sensitive_tag_overlap = defaultdict(list)
    resistant_tag_overlap = defaultdict(list)

    stress_ontology, category = DatabaseUtils.load_tolerance_ontology()
    root_category_count = defaultdict(int)

    for key in category:

        (root, molecule_class) = category[key]
        for molecule in molecule_class:
            root_category_count[molecule] += 1

    for key in root_category_count:
        print key, root_category_count[key]

    for paper in papers:

        for m in paper.mutants:

            observed_mutations = set()

            for j in m.mutations:

                gene_name = j.name.upper()
                geneCount = geneCount + 1
                counting[gene_name] = counting[gene_name] + 1
                observed_mutations.add(gene_name)

                for sensitive_phenotype in m.sensitive_phenotypes:

                    (root_class,temp_tags) = category[sensitive_phenotype.upper()]

                    passes_threshold = False
                    for molecule in temp_tags:
                        if(root_category_count[molecule] > threshold):
                            passes_threshold = True

                    if(root_class.upper() in target_categories and passes_threshold):
                        provided_tags = temp_tags
                    else:
                        continue

                    for tag in provided_tags:
                        sensitive_tag_overlap[tag].append(gene_name)

                for tol_phenotype in m.resistant_phenotypes:

                    (root_class,temp_tags) = category[tol_phenotype.upper()]

                    passes_threshold = False
                    for molecule in temp_tags:
                        if(root_category_count[molecule] > threshold):
                            passes_threshold = True

                    if(root_class.upper() in target_categories and passes_threshold):
                        provided_tags = temp_tags
                    else:
                        continue

                    for tag in provided_tags:
                        resistant_tag_overlap[tag].append(gene_name)
    count_threshold = 0

    counting_data = []

    for key in counting:

        if(counting[key] >= count_threshold):
            counting_data.append((key, counting[key]))

    counting_data = sorted(counting_data, reverse=True, key=lambda tup: tup[1])
    keys = [item[0] for item in counting_data]
    values = [item[1] for item in counting_data]

    print '\nModified gene/interaction report'

    print 'A total of unique %i genes are mutated in %i LASER records.' % (len(keys), len(papers))
    print 'A total of %i (non-unique) genes are mutated in LASER' % geneCount
    print 'The top 20 modified genes comprise %f percent of all modified genes.' % (float(numpy.sum(values[0:19]))/float(numpy.sum(values)) * 100.0)

    rows = set(resistant_tag_overlap.keys()) | set(sensitive_tag_overlap.keys())

    tol_abbrevs = DatabaseUtils.load_tolerance_abbrevs()

    matrix, rows, resistant_sig_tuple = interaction_matrix(resistant_tag_overlap,rows, genome_size=genome_size)

    #output heatmap
    glib.generate_heatmap(numpy.array(matrix), rows, 'log2[P-value]', os.path.join(DatabaseUtils.OUTPUT_DIR, output_prefix+'Resistance Interaction HeatMap.pdf'), mapper=tol_abbrevs)

    matrix, rows, sensitive_sig_tuple = interaction_matrix(sensitive_tag_overlap,rows, genome_size=genome_size)

    glib.generate_heatmap(numpy.array(matrix), rows, 'log2[P-value]', os.path.join(DatabaseUtils.OUTPUT_DIR,output_prefix+'Sensitive Interaction HeatMap.pdf'), mapper=tol_abbrevs)


    return resistant_sig_tuple, sensitive_sig_tuple

def association_figure_tags(papers, genome_size, output_prefix = '', gene_transformer = None):

    def convert_entries(source_dict, converter):

        output_dict = defaultdict(set)

        for key, value in source_dict.iteritems():

            for v in value:
                if(v in converter):
                    output_dict[key].update(converter[v])

        return output_dict

    #gene transformer is a dict of str:->list, where each gene is transformed into at least one alternate id (I originally intended this to allow
    #go tags to used instead of gene names

    #todo: make gene-gene overlap figure.
    #we want to run extract a set containing every gene name in LASER. However, we need to process this a bit to remove brackets.
    processed = set()

    counting = defaultdict(int)

    geneCount = 0
    sensitive_tag_overlap = defaultdict(set)
    resistant_tag_overlap = defaultdict(set)

    stress_ontology, category = DatabaseUtils.load_tolerance_ontology()

    tags = {}
    chemicals = set()
    ontology_additions = []
    
    for paper in papers:

        for m in paper.mutants:

            observed_mutations = set()
            skip = False

            for chemical in m.tolerance:

                if(chemical.upper() not in stress_ontology):
                    ontology_additions.append(chemical.upper())
                    skip = True

                chemicals.add(chemical)

                tags[chemical] = paper.categories

            if(skip):
                continue

            for j in m.mutations:

                gene_name = j.name.upper()
                geneCount = geneCount + 1
                counting[gene_name] = counting[gene_name] + 1
                observed_mutations.add(gene_name)

                for sensitive_phenotype in m.sensitive_phenotypes:
                    provided_tags = stress_ontology[sensitive_phenotype.upper()]
                    for tag in provided_tags:
                        sensitive_tag_overlap[tag].add(gene_name)

                for tol_phenotype in m.resistant_phenotypes:
                    provided_tags = stress_ontology[tol_phenotype.upper()]
                    for tag in provided_tags:
                        resistant_tag_overlap[tag].add(gene_name)

    if(len(ontology_additions) != 0):

        fhandle = open('Ontology additions.txt','w')

        for line in set(ontology_additions):
            fhandle.write(line + '\n')
        fhandle.close()

        raise AssertionError('Update the stress ontology.')


    if(gene_transformer != None):

        sensitive_tag_overlap = convert_entries(sensitive_tag_overlap, gene_transformer)
        resistant_tag_overlap = convert_entries(resistant_tag_overlap, gene_transformer)

#    bipartite_analysis(resistant_tag_overlap)

    #output modified gene distribution
    count_threshold = 0

    counting_data = []

    for key in counting:

        if(counting[key] >= count_threshold):
            counting_data.append((key, counting[key]))

    counting_data = sorted(counting_data, reverse=True, key=lambda tup: tup[1])
    keys = [item[0] for item in counting_data]
    values = [item[1] for item in counting_data]

    print '\nModified gene/interaction report'

    print 'A total of unique %i genes are mutated in %i LASER records.' % (len(keys), len(processed))
    print 'A total of %i (non-unique) genes are mutated in LASER' % geneCount 
    print 'The top 20 modified genes comprise %f percent of all modified genes.' % (float(numpy.sum(values[0:19]))/float(numpy.sum(values)) * 100.0)

    rows = set(resistant_tag_overlap.keys()) | set(sensitive_tag_overlap.keys())

    tol_abbrevs = DatabaseUtils.load_tolerance_abbrevs()

    matrix, rows, resistant_sig_tuple = interaction_matrix(resistant_tag_overlap,rows, genome_size=genome_size)

    glib.generate_heatmap(numpy.array(matrix), rows, 'log2[P-value]', os.path.join(DatabaseUtils.OUTPUT_DIR, output_prefix+'Resistance Interaction HeatMap.pdf'), mapper=tol_abbrevs)

    matrix, rows, sensitive_sig_tuple = interaction_matrix(sensitive_tag_overlap,rows, genome_size=genome_size)

    glib.generate_heatmap(numpy.array(matrix), rows, 'log2[P-value]', os.path.join(DatabaseUtils.OUTPUT_DIR,output_prefix+'Sensitive Interaction HeatMap.pdf'), mapper=tol_abbrevs)

    return resistant_sig_tuple, sensitive_sig_tuple


'''
# number of papers
# species count (e. coli/s. cerevisiae)
# unique products
# journals
# methods (overall)
# methods (mutants)
# gene sources
# designs with yield/titer information
# carbon sources
#number of mutants per paper (avg/std)
#number of mutations per mutant (avg/std)
average mutations per design per year (maybe combine with papers per year?)
'''

def vital_statistics(papers, parserMap):

    def dict_to_paired_array(dictionary):

        tphen = []

        for key in dictionary:
            tphen.append((key, dictionary[key]))

        tphen = sorted(tphen, key=lambda x: x[1], reverse=True)

        tolerance_keys = [item[0] for item in tphen]
        tolerance_vals = [item[1] for item in tphen]

        return tolerance_keys, tolerance_vals

    year_papers = defaultdict(int)
    year_mutation = defaultdict(list)
    year_models = defaultdict(list)

    method_counts = defaultdict(int)
    mutation_counts = defaultdict(int)

    mutantCounts = []
    mutationCounts = []

    total_models = []

    numEcoli = 0
    numYeast = 0
    numOther = 0

    gene_usage = defaultdict(int)

    tolerance_phenotypes = defaultdict(int)

    specific_phenotypes = set()

    ecoli_genes = 0

    #compute number mutants/average stats, etc
    for paper in papers:

        for cat in paper.categories:
            tolerance_phenotypes[cat]+=len(paper.mutants)

        year_papers[paper.year]+=1

        total_models.append(paper.total_designs)
        year_models[paper.year].append(len(paper.mutants))

        mutantCounts.append(len(paper.mutants))
        for mutant in paper.mutants:

            specific_phenotypes.update(mutant.resistant_phenotypes)
            specific_phenotypes.update(mutant.sensitive_phenotypes)

            for method in mutant.methods:
                method_counts[method]+=1

            if(mutant.species.upper() == 'Escherichia coli'.upper()):
                numEcoli+=1
            elif(mutant.species.upper() == 'Saccharomyces cerevisiae'.upper()):
                numYeast+=1
            else:
                numOther+=1

            mutationCounts.append(len(mutant.mutations))
            year_mutation[paper.year].append(len(mutant.mutations))

            for mutation in mutant.mutations:

                if(mutation.source.upper() == 'Escherichia coli'.upper()):
                    ecoli_genes+=1

                gene_usage[mutation.name] +=1
                for change in mutation.changes:
                    mutation_counts[change] +=1


    gene_output = []

    for (gene, count) in gene_usage.iteritems():
        gene_output.append((gene, count))

    go = sorted(gene_output, key=lambda x: x[1], reverse=True)

    go_values = [x[1] for x in go]

    topX = 25

    fraction = float(sum(go_values[0:topX-1]))/float(sum(go_values))

    print 'The top %i genes represent %f fraction of all mutations (ratio of actual to expected is %f)' % (topX, fraction, fraction/(float(topX)/float(len(gene_usage.keys()))))

    go = go[0:topX-1]

    glib.simple_bargraph([x[0] for x in go], [x[1] for x in go],'Mutated Genes','Mutation Counts', os.path.join(DatabaseUtils.OUTPUT_DIR,'Mutated Gene Counts.pdf'), rotation='vertical')

    #output distribution of gene uses

    glib.histogram(gene_usage.values(),'Distribution of Gene Usage Counts','Counts',os.path.join(DatabaseUtils.OUTPUT_DIR,'Gene Usage Distribution.pdf'), useMedianFilter=True)

    tolerance_keys, tolerance_vals = dict_to_paired_array(tolerance_phenotypes)
    tolerance_mapper = DatabaseUtils.load_tolerance_abbrevs()
    glib.simple_bargraph(tolerance_keys, tolerance_vals,'Tolerance Phenotype','Mutant Counts', os.path.join(DatabaseUtils.OUTPUT_DIR,'Tolerance Counts.pdf'), rotation='vertical', mapper=tolerance_mapper)

    method_keys, method_vals = dict_to_paired_array(method_counts)
    glib.simple_bargraph(method_keys, method_vals,'Engineering Method','Mutant Counts', os.path.join(DatabaseUtils.OUTPUT_DIR,'Method Counts.pdf'), rotation='vertical', mapper=None)

    mutation_keys, mutation_vals = dict_to_paired_array(mutation_counts)
    mutation_mapper = DatabaseUtils.load_mutation_abbrevs()
    glib.simple_bargraph(mutation_keys, mutation_vals,'Mutation Type','Counts', os.path.join(DatabaseUtils.OUTPUT_DIR,'Mutation Counts.pdf'), rotation='vertical', mapper=mutation_mapper)

    paper_by_year = []
    mutations_by_year = []

    chronological = sorted(year_papers.keys())

    for year in chronological:
        paper_by_year.append((year, year_papers[year]))
        mutations_by_year.append((year, numpy.max(year_models[year])))

    #papers/mutations per year
    paper_values = [item[1] for item in paper_by_year]
    mutation_values = [item[1] for item in mutations_by_year]
    glib.linebar(chronological, paper_values, chronological, mutation_values, 'Publication Year', 'Resistome Papers', 'Mutants/Paper', DatabaseUtils.OUTPUT_DIR+'Papers and Mutants Per Year.pdf')

    mutants_per_paper_avg = numpy.mean(mutantCounts)
    mutants_per_paper_std = numpy.std(mutantCounts)

    mutations_per_design_avg = numpy.mean(mutationCounts)
    mutations_per_design_std = numpy.std(mutationCounts)
    
    numPapers = len(papers)

    numJournals = len(distribution('journal',0,papers,parserMap)[0].keys())
    numDesignMethods = len(distribution('design_method',0,papers,parserMap)[0].keys())
    numMutantMethods = len(distribution('methods',0,papers,parserMap)[0].keys())
    numGeneSources = len(distribution('source',0,papers,parserMap)[0].keys())

    print '\nVital statistics report for the Resistome'

    vital_stats = []

    #vital_stats.append('Number of total mutants per paper: %f' % (total_per_paper_med))
    vital_stats.append('Number of recorded mutants per paper: %f (%f)' % (mutants_per_paper_avg, mutants_per_paper_std))
    vital_stats.append('Number of mutations per design %f (%f)' % (mutations_per_design_avg, mutations_per_design_std))
    vital_stats.append('Number of unique papers %i' % numPapers)
    vital_stats.append('Number of E. coli designs %i' % numEcoli)
    vital_stats.append('Number of yeast designs %i' % numYeast)
    vital_stats.append('Number of other designs %i' % numOther)
    vital_stats.append('Number of Journals: %i' % numJournals)
    vital_stats.append('Number of unique phenotypes (both resistance + sensitivity) %i' % len(specific_phenotypes))
    vital_stats.append('Number of phenotype categories: %i' % len(tolerance_phenotypes.keys()))
    vital_stats.append('Number of E. coli genes: %i' % ecoli_genes)
    vital_stats.append('Number of unique gene sources %i' % numGeneSources)
    vital_stats.append('Number of unique genes %i' % len(gene_usage.keys()))
    vital_stats.append('Number of project design methodologies: %i' % numDesignMethods)
    vital_stats.append('Number of mutant engineering/optimization techniques: %i' % numMutantMethods)

    fhandle = open(os.path.join(DatabaseUtils.OUTPUT_DIR,'Vital Statistics Report.txt'),'w')

    for line in vital_stats:
        print line
        fhandle.write(line + '\n')

    fhandle.close()

    print '\nFinished vital statistics report.'

def modification_stats(papers):

    native_mutations = 0
    hetero_mutations = 0

    native_dist = defaultdict(int)
    hetero_dist = defaultdict(int)

    native_genes = defaultdict(int)
    hetero_genes = defaultdict(int)

    mutation_count = 0

    for paper in papers:

        for mutant in paper.mutants:

            mutations = mutant.mutations

            mutation_count = mutation_count + len(mutations)

            host = paper.species

            for j in mutations:

                name = j.name.upper()
                source = j.source
                mutations_list = j.changes

                if(source.upper() == host.upper()):
                    native_mutations = native_mutations + 1
                    
                    for mut in mutations_list:
                        native_dist[mut] = native_dist[mut] + 1

                    native_genes[name] = native_genes[name] + 1
                else:
                    hetero_mutations = hetero_mutations + 1

                    for mut in mutations_list:
                        hetero_dist[mut] = hetero_dist[mut] + 1

                    hetero_genes[name] = hetero_genes[name] + 1

    filter_native_dist = defaultdict(int)
    filter_hetero_dist = defaultdict(int)

    keys = set(native_dist.keys()) | set(hetero_dist.keys())

    nvalues = []
    hvalues = []

    mut_mapper = DatabaseUtils.load_mutation_abbrevs()

    for key in keys:
        if(key in mut_mapper):
            filter_native_dist[key] = filter_native_dist[key] + native_dist[key]
            filter_hetero_dist[key] = filter_hetero_dist[key] + hetero_dist[key]

    keys = set(filter_native_dist.keys()) | set(filter_hetero_dist.keys())

    for key in keys:
        nvalues.append((key, filter_native_dist[key]))
        
    nvalues = sorted(nvalues, key=lambda nv: nv[1], reverse=True)

    keys = [item[0] for item in nvalues]

    print 'Number of total genes mutations: %i' % mutation_count

    glib.multi_bargraph([keys, keys], [[nvalue[1] for nvalue in nvalues], hvalues], ['Native','Heterologous'], 'Mutation Types', 'Count', os.path.join(DatabaseUtils.OUTPUT_DIR,'Native vs. Heterologous Mutations.pdf'), rotation='vertical', mapper = mut_mapper)

#barcharts as needed for various things. Might need to combine some of these.
def simple_figures(papers, parserMap):

    #get journal distribution
    journals, keys, values =  distribution('journal', 5, papers, parserMap)

    journal_mapper = DatabaseUtils.load_journal_abbrevs()
    
    glib.simple_bargraph(keys, values, 'Journals', 'Count', DatabaseUtils.OUTPUT_DIR+'Journal Distribution.pdf', mapper=journal_mapper)

    #methods used for mutant design figure
    methods, keys, values =  distribution('methods', 10, papers, parserMap)

    method_mapper = {}
    method_mapper['human'.upper()] = 'HUM'
    method_mapper['library'.upper()] = 'LIB'
    method_mapper['fba'.upper()] = 'FBA'
    method_mapper['evolution'.upper()] = 'EVO'
    method_mapper['random mutagenesis'.upper()] = 'RAND'
    method_mapper['protein engineering'.upper()] = 'PREN'
    method_mapper['transcriptomics'.upper()] = 'TRANS'
    method_mapper['liquidliquid'.upper()] = 'LL'
    method_mapper['computational'.upper()] = 'COMP'
    method_mapper['Other'] = 'OTH'

    glib.simple_bargraph(keys, values, 'Design Methods', 'Count', DatabaseUtils.OUTPUT_DIR+'Method Distribution.pdf', mapper = method_mapper)
    
    #mutations made figure
    mutations, keys, values =  distribution('changes', 20, papers, parserMap)

    glib.simple_bargraph(keys, values, 'Mutation Types', 'Count', DatabaseUtils.OUTPUT_DIR+'Mutation Distribution.pdf', mapper = DatabaseUtils.load_mutation_abbrevs())

def tag_distribution(papers, output_file_name, top = 9):

    category_counter = defaultdict(dict)

    frequency = defaultdict(int)

    type_set = set()

    for paper in papers:

        for tag in paper.categories:

            type_set.add(tag)

            frequency[tag] += 1

            if(tag in category_counter[paper.year]):
                category_counter[paper.year][tag] = category_counter[paper.year][tag]+1
            else:
                category_counter[paper.year][tag]=1

    glib.generate_stacked(frequency, category_counter, os.path.join(DatabaseUtils.OUTPUT_DIR,output_file_name), top=top, name_mapper=DatabaseUtils.load_tolerance_abbrevs(), cmap='rainbow')

def gene_growth_frequency(papers, standard_obj, filename, growth_threshold, non_growth_threshold):

    non_growth = 'Non-Growth'
    growth     = 'Growth'


    def classifier(tag):

        tag = tag.upper()

        if(tag == 'general_growth'.upper()):
            return growth
        else:
            return non_growth

    tags,tolerance_ontology = DatabaseUtils.load_tolerance_ontology()

    growth_ratio = defaultdict(dict)
    frequency = defaultdict(int)

    for paper in papers:
        for mutant in paper.mutants:
            for mutation in mutant.mutations:

                frequency[mutation.name] += 1

                if(mutation.name not in growth_ratio):
                    growth_ratio[mutation.name][growth] = 0
                    growth_ratio[mutation.name][non_growth] = 0

                for p in mutant.resistant_phenotypes:
                    general_tags = tags[p.upper()]
                    for t in general_tags:
                        growth_ratio[mutation.name][classifier(t)]+=1

                for p in mutant.sensitive_phenotypes:
                    general_tags = tags[p.upper()]
                    for t in general_tags:
                        growth_ratio[mutation.name][classifier(t)]+=1

    key_order = [growth, non_growth]

    normalized_dict = defaultdict(dict)

    for key in growth_ratio:

        #print growth_ratio[key][growth], growth_ratio[key][non_growth], key

        normalized_dict[key][growth] = float(growth_ratio[key][growth]) / (float(growth_ratio[key][growth] + float(growth_ratio[key][non_growth])))
        normalized_dict[key][non_growth] = 1.0 - normalized_dict[key][growth]

    growth_associated_genes = set()

    non_growth_associated = set()

    for gene in normalized_dict:

        if(normalized_dict[gene][growth] > growth_threshold):
            growth_associated_genes.add(gene)

        if(normalized_dict[gene][non_growth] > non_growth_threshold):
            non_growth_associated.add(gene)

    print 'Found the following genes at a >%f threshold of growth associated improvement' % growth_threshold

    print 'Fraction of genes meeting this growth threshold %f \n' % (float(len(growth_associated_genes)) / float(len(normalized_dict.keys())))


    print 'Found the following genes at a >%f threshold of non-growth associated improvement' % non_growth_threshold

    print 'Fraction of genes meeting this non-growth threshold %f' % (float(len(non_growth_associated)) / float(len(normalized_dict.keys())))

    #glib.histogram([normalized_dict[x][growth]*100.0 for x in normalized_dict],'% associated with growth','Count','test.pdf')

    glib.plotcdf([normalized_dict[x][growth]*100.0 for x in normalized_dict],'Growth Associated (%)','Cumulative Fraction','Growth', filename)

    gene_ontology_analysis('Growth associated genes (threshold=%f).txt' % growth_threshold,growth_associated_genes,standard_obj,'Escherichia coli')

    gene_ontology_analysis('Non-Growth associated genes (threshold=%f).txt' % non_growth_threshold,non_growth_associated,standard_obj,'Escherichia coli')

    return growth_associated_genes, non_growth_associated

    #glib.generate_stacked(frequency,growth_ratio,'Test.pdf',add_other=False)

def protein_data(papers, protein_name, pdb_file_location):

    mutation_objects = []

    for paper in papers:
        for mutant in paper.mutants:
            for mutation in mutant.mutations:
                if(mutation.name.upper() == protein_name.upper()):
                    mutation_objects.append(mutation)

    combined_annotations = defaultdict(list)

    for mutation in mutation_objects:

        for change in mutation.changes:

            combined_annotations[change].extend([x.strip() for x in mutation.annotation(change).split(',')])

    print 'Number of amino acid changes: %i' % len(combined_annotations['aa_snps'])

    fhandle = open(pdb_file_location,'rU')

    residue_to_atom_mapping = {}

    atom_changes = set()

    for line in fhandle:
        tokens = line.strip().split()
        if('ATOM' in tokens[0]):
            residue_to_atom_mapping[tokens[5]] = tokens[1]

    for aa in combined_annotations['aa_snps']:

        residue_number = aa[1:-2]

        if(residue_number in residue_to_atom_mapping):
            atom_changes.add(residue_to_atom_mapping[residue_number])
        else:
            print 'is %i supposed to be in the atom mapping?' % int(residue_number)


    #print ' '.join(atom_changes)

def gene_gene_interaction_network(papers, weight_threshold = 0):

    import itertools

    edges = set()

    weights = defaultdict(int)
    node_frequency = defaultdict(int)

    for paper in papers:
        for mutant in paper.mutants:

            #list of all affected genes in the mutant
            genes = mutant.affected_genes

            cartesian_product = itertools.product(genes, genes)

            for (x, y) in cartesian_product:
                if(x != y):
                    edges.add((x,y))
                    #edges.add((y,x))
                    weights[(x,y)]+=1
                    weights[(y,x)]+=1
                    node_frequency[x]+=1
                    node_frequency[y]+=1


    edge_tuples = []
    for (x,y) in edges:

        if(weights[(x,y)]/2 > weight_threshold):
            edge_tuples.append((x,y,{'weight': weights[(x,y)]/2}))
            #edge_tuples.append((x,y))

    for node in node_frequency:
        node_frequency[node] = node_frequency[node]/2

    G = networkx.Graph()
    G.add_edges_from(edge_tuples)

    return G, node_frequency

    #glib.plot_network(G,'test','test_gi.pdf')


def network_changes(papers, standard_obj, tolerate_errors = True, weight_filter = None):

    from RegulatoryBuilder import RegulatoryBuilder

    rb = RegulatoryBuilder(standard_obj)

    ggin, node_frequency  = gene_gene_interaction_network(papers, weight_threshold = 2)

    nodes_to_clusters,clusters_to_nodes = DatabaseUtils.compute_igraph_clustering(ggin,method='multilevel')

    cmap = glib.get_cmap(len(clusters_to_nodes), cmap='rainbow')

    for node in ggin.nodes():

        ggin.node[node]['color'] = cmap(nodes_to_clusters[node])
        ggin.node[node]['weight'] = float(node_frequency[node])/float(max(node_frequency.values())) * 200.0 + 50

    glib.plot_network(ggin, 'test', os.path.join(DatabaseUtils.OUTPUT_DIR,'Genetic Interaction Network.pdf'), use_node_weights=True)

    go_dict = None
    species_dict = None

    print 'Number of nodes in gene-gene interaction network: %i, number of edges: %i' %(len(ggin.nodes()),len(ggin.edges()))

    for c in clusters_to_nodes:

        _,go_dict,species_dict=gene_ontology_analysis(str(c) + '_cluster_GI report.txt', clusters_to_nodes[c], standard_obj, 'Escherichia coli', GO_dict=go_dict, species_dict=species_dict, threshold=1e-5)

    node_weights = defaultdict(int)

    for paper in papers:

        for mutant in paper.mutants:

            for mutation in mutant.mutations:

                if(len(mutation.changes) == 0):
                    raise AssertionError(paper.title)
                node_weights[mutation.name.upper()] +=1

    threshold = 1

    modified_network, missing_nodes = rb.apply_weights(rb.mapper['Escherichia coli'.upper()],node_weights, standard_obj, species='Escherichia coli', propagate_weights = False, filter_function = lambda x: True if abs(x) > threshold else False)

    if(len(missing_nodes) > 0):

        if(tolerate_errors):
            warnings.warn('Number of nodes not found in regulonDB or SGD regulatory networks: %i' % len(missing_nodes))
        else:

            print 'Missing nodes...'

            for node in missing_nodes:
                print node

            raise AssertionError('Number of nodes not found in regulonDB or SGD regulatory networks: %i' % len(missing_nodes))

    for node in modified_network.nodes():

        if(modified_network.out_degree(node) > 0):
            modified_network.node[node]['color'] = 'c'
        else:
            modified_network.node[node]['color'] = 'm'

    glib.plot_network(modified_network.to_undirected(), 'Test', os.path.join(DatabaseUtils.OUTPUT_DIR,'Regulatory Propagation Network.pdf'),use_node_weights=True, scaling_factor=10)

    modified_Interaction_network, missing_nodes = rb.apply_weights(rb.interaction_mapper['Escherichia coli'.upper()],node_weights, standard_obj, species='Escherichia coli', propagate_weights = False, filter_function = lambda x: True if abs(x) > threshold else False)

    for i_node in modified_Interaction_network.nodes():

        converted_names = standard_obj.revert(i_node)

        for node_x in converted_names:

            if(node_x not in modified_network):
                modified_Interaction_network.node[node_x]['color'] = 'y'
                continue

            if(modified_network.out_degree(node_x) > 0):
                modified_Interaction_network.node[node_x]['color'] = 'c'
            else:
                modified_Interaction_network.node[node_x]['color'] = 'm'

    glib.plot_network(modified_Interaction_network.to_undirected(), 'Test', os.path.join(DatabaseUtils.OUTPUT_DIR,'Interaction Propagation Network.pdf'),use_node_weights=True, scaling_factor=10)


def random_versus_designed_comparison(papers, essential_genes_list, gene_ontology_mapper):

    random_papers = []
    nonrandom_papers = []

    def compute_stats_of_interest(paper_list):

        #number of mutation types (distribution, possibly)
        #number of genes mutated per study (trivial)
        #number of go processes affected by mutations, probably on a study basis
        #number of essential genes affected

        mutation_types = []
        mutated_gene_counts  = []
        mutated_go_processes = []
        essential_gene_counts = []

        for paper in paper_list:
            for mutant in paper.mutants:

                mutated_gene_counts.append(len(mutant.affected_genes))

                go_set = set()

                essential_count = 0

                for gene in mutant.affected_genes:

                    if(gene in essential_genes_list):
                        essential_count+=1

                    if(gene in gene_ontology_mapper):
                        go_set.update(gene_ontology_mapper[gene])

                mutated_go_processes.append(len(go_set))
                essential_gene_counts.append(essential_count)

                mutation_types.append(len(mutant.mutations))

        return (mutation_types, mutated_gene_counts, mutated_go_processes, essential_gene_counts)

    for paper in papers:

        if('random' in paper.design_method):
            random_papers.append(paper)
        else:
            nonrandom_papers.append(paper)

    print 'Number of random papers in LASER: %i out of %i papers (%f)' % (len(random_papers), len(papers), float(len(random_papers))/float(len(papers)))

    (random_types, random_gene_counts, random_go_processes, random_essential) = compute_stats_of_interest(random_papers)
    (nr_types, nr_gene_counts, nr_go_processes, nr_essential)                 = compute_stats_of_interest(nonrandom_papers)

    print 'Average number of mutational types per random, non-random studies: %f, %f, P-value (t-test, two-tailed) = %f' % (numpy.average(random_types), numpy.average(nr_types), scipy.stats.ttest_ind(random_types, nr_types)[1])

    print 'Average number of mutated genes per random, non-randomly mutated strain: %f, %f, P-value (t-test, two-tailed): %f' % (numpy.average(random_gene_counts), numpy.average(nr_gene_counts), scipy.stats.ttest_ind(random_gene_counts, nr_gene_counts)[1])

    print 'Average number of gene ontology processes changed per random, non-randomly mutated strain: %f, %f, P-value (t-test, two-tailed): %f' % (numpy.average(random_go_processes), numpy.average(nr_go_processes), scipy.stats.ttest_ind(random_go_processes, nr_go_processes)[1])

    print 'Average number of essential genes modified per random, non-random strain: %f, %f, P-value (t-test, two-tailed): %f' % (numpy.average(random_essential), numpy.average(nr_essential), scipy.stats.ttest_ind(random_essential, nr_essential)[1])

    #add mutational spectra bar chart here


def generate_go_report(title, GO_dict, species_dict, go_to_gene, number_of_genes, counting_stats, random_seed, threshold = 1e-6):

    #todo: add seed to function for reproducibility

    p_values = gene_ontology.compute_enrichment(species_dict, number_of_genes,counting_stats, seed = random_seed)

    output_file = []

    nodes_to_retain = set()

    for key in p_values:
        if(p_values[key] < threshold):
            output_file.append((key, p_values[key], GO_dict[key]['name'], ','.join(go_to_gene[key])))
            nodes_to_retain.add(key)
            #print key, p_values[key], GO_dict[key]['name'], ','.join(go_to_gene[key])

    if(len(output_file) > 0):

        fhandle = open(os.path.join(DatabaseUtils.OUTPUT_DIR,title+'_GO_statistics.txt'),'w')

        for line in output_file:

            line = [str(x) for x in line]

            fhandle.write('\t'.join(line) + '\n')
        fhandle.close()

    else:

        print 'No GO terms reached the specified significance threshold.'

    return p_values

def gene_ontology_analysis(title, gene_list, standard_obj, species, go_source_data = 'go-basic.obo', threshold=1e-6, GO_dict=None, species_dict = None):

    def build_GO_datasets(species_source, go_source, data_key):

        GO_dict, GO_network = gene_ontology.parse_GO_file(go_source,data_key)

        species_dict = gene_ontology.load_species_dataset(species_source,standard_obj,species)

        return GO_dict, species_dict, GO_network

    if(species.upper() == 'Escherichia coli'.upper()):
        species_path = 'gene_association.ecocyc'
    if(species.upper() == 'Saccharomyces cerevisiae'.upper()):
        species_path = 'gene_association.sgd'

    if(species_path == None):
        raise AssertionError('You need to provide a known species type to load gene ontology data: %s is not valid.' % species)

    filtered_designs = []

    if(GO_dict == None or species_dict == None):
        GO_dict, species_dict, _ = build_GO_datasets(os.path.join(DatabaseUtils.INPUT_DIR,species_path),os.path.join(DatabaseUtils.INPUT_DIR,go_source_data),'name')


    design_go_list = []

    go_to_gene = defaultdict(set)

    for gene in gene_list:

        if(gene.upper() in species_dict):
            design_go_list.append((gene.upper(), species_dict[gene.upper()]))
            for go in species_dict[gene.upper()]:
                go_to_gene[go].add(gene.upper())

    #print len(design_go_list), len(set([x[0] for x in design_go_list]))

    _, counting_stats = gene_ontology.gene_set_statistics(GO_dict,design_go_list,'name')

    return generate_go_report(title,GO_dict,species_dict,go_to_gene,len(design_go_list),counting_stats,50,threshold=threshold), GO_dict,species_dict

def recommendation_system_figure(papers, reduced_ontology, go_network):

    import RecommendationSystem

    vector_set, all_features = RecommendationSystem.generate_vector_set(papers, 'ontology', include_missing=False, feature_converter=reduced_ontology)

    reduced_set = []

    for v in vector_set:
        if(len(v) > 1):
            reduced_set.append(v)

    pd = RecommendationSystem.pairwise_distance_matrix(reduced_set,go_network)

    matrix = []

    keys = sorted(pd.keys())

    for key_x in keys:

        temp = []
        for key_y in keys:

            if(key_x==key_y):
                temp.append(0)
            else:
                #this is not typically necessary, but it is just for display (10 is quite far anyway).
                temp.append(min(10.0,max(pd[key_x][key_y],1.0)))

        matrix.append(temp)

    glib.generate_heatmap(numpy.array(matrix),keys,'Vector Distance','test.pdf',use_labels=False, cmap='rainbow')

def interaction_analysis(papers, standard_obj, ecoli_gene_ontology):

    go_genome_size = set()

    for key in ecoli_gene_ontology:
        go_genome_size.update(ecoli_gene_ontology[key])

    resistant_tuples, sensitive_tuples = association_figure_tags(papers,genome_size=len(go_genome_size), output_prefix='GeneOntologyBased_',gene_transformer=ecoli_gene_ontology)

    resistant_tuples, sensitive_tuples = association_figure_tags(papers,genome_size=4377, output_prefix='TagBased_')



def paper1_figures():

    #MG1655's genome has 4377 genes.


    standard_obj = Standard(os.path.join(DatabaseUtils.INPUT_DIR,'Gene Name Mapping.txt'))

    phenotype_obj = Standard(os.path.join(DatabaseUtils.INPUT_DIR, 'Compound Information.txt'))

    papers, parserMap = DatabaseUtils.getDatabase(standard_obj, phenotype_obj, getParser = True)

    ecoli_essential_genes = DatabaseUtils.load_essential_genes(standard_obj, 'Escherichia coli')

    _,go_network = gene_ontology.parse_GO_file(os.path.join(DatabaseUtils.INPUT_DIR,'go-basic.obo'),'name')

    ecoli_gene_ontology = gene_ontology.load_species_dataset(os.path.join(DatabaseUtils.INPUT_DIR,'gene_association.ecocyc'),standard_obj,'Escherichia coli')

    ecoli_gene_ontology = gene_ontology.most_specific_tags_only(ecoli_gene_ontology, go_network)

    #protein_data(papers, 'crp','C:\\Users\\jdwinkler\\OneDrive\\Gill Work\\Repositories\\resistome analysis\\pdb\\2GZW.pdb')

    #tag_distribution(papers,'Distribution of Resistome Tags.pdf', top=12)

    #product_gene_freq(papers)

    #vital_statistics(papers, parserMap)

    #simple_figures(papers,parserMap)

    #network_changes(papers, standard_obj, weight_filter=None)

    #interaction_analysis(papers, standard_obj, ecoli_gene_ontology)

    #random_versus_designed_comparison(papers, ecoli_essential_genes, ecoli_gene_ontology)

    #gene_growth_frequency(papers, standard_obj, os.path.join(DatabaseUtils.OUTPUT_DIR,'Growth CDF Figure.pdf'), growth_threshold=0.9, non_growth_threshold=0.9)

    #gene_gene_interaction_network(papers)

    recommendation_system_figure(papers,ecoli_gene_ontology,go_network)

if(__name__ == '__main__'):
    paper1_figures()

