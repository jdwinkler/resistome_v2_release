import os
import numpy
import scipy
import scipy.stats
from scipy.optimize import curve_fit
from collections import defaultdict

def generate_stacked(frequency, year_breakdown, filename, top = 9, name_mapper = None, add_other = True, cmap = 'jet'):

    freq_array = []

    if(name_mapper == None):
        name_mapper = {}

    for key in frequency:
        freq_array.append((key, frequency[key]))

    #sort to get descending values
    freq_array = sorted(freq_array, key=lambda freq_array: freq_array[1], reverse=True)

    #top X entries
    if(top != -1):
        top_types = [d[0] for d in freq_array[0:top-1]]
    else:
        top_types = [d[0] for d in freq_array]

    #make a stacked bar chart with products

    keys = year_breakdown.keys()
    bar_data = []
    key_order = sorted(keys)
    type_set = set()

    for key in key_order:

        data = defaultdict(int)

        for ptype in year_breakdown[key]:

            if(ptype in top_types):
                data[ptype]+=1
                type_set.add(ptype)
            else:
                data['other'] += 1

        value_array = []

        for ptype in data:
            value_array.append(data[ptype])

        sum_types = numpy.sum(value_array)

        value_array = []

        for ptype in data:
            data[ptype] = float(data[ptype])/float(sum_types)
            value_array.append(data[ptype])

        bar_data.append(data)

    order = sorted([item for item in list(type_set)])

    if(add_other and 'other' not in order):
        order.append('other')

    stackedbar(order, bar_data, key_order, 'Year','Proportion', filename, rotation='vertical', mapper=name_mapper, cmap=cmap)

def get_cmap(N, cmap = 'jet'):

    import matplotlib.cm as cmx
    import matplotlib.colors as colors
    '''Returns a function that maps each index in 0, 1, ... N-1 to a distinct 
    RGB color.'''
    color_norm  = colors.Normalize(vmin=0, vmax=N-1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap=cmap)
    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)
    return map_index_to_rgb_color

def plot_network(G, title, filename, use_node_weights = False, use_edge_weights = False, use_labels = False, use_title=False, default_size = 300, scaling_factor = 1.0):

    import networkx
    import matplotlib.pyplot as plt

    k = 1.5 / numpy.sqrt(len(G.nodes()))

    scale = 10

    pos = networkx.spring_layout(G, k=k, scale = scale)
    #pos = networkx.circular_layout(G,scale = scale)

    #node_colors = {}

    node_color_test = []

    node_size_test = []

    for node in G.nodes():

        if('color' in G.node[node]):
            #node_colors[node] = G.node[node]['color']
            node_color_test.append(G.node[node]['color'])
        else:
            #default color is red (in pajek, matplotlib, etc)
            #node_colors[node] = 'r'
            node_color_test.append('r')

        if(use_node_weights):
            node_size_test.append(abs(G.node[node].get('size',G.node[node].get('weight',default_size)))*scaling_factor)
        else:
            node_size_test.append(default_size)

    if(use_edge_weights == True):

        edge_weights = []

        for edge in G.edges():
            edge_weights.append(G[edge[0]][edge[1]]['weight'] * scaling_factor)

        networkx.draw_networkx_edges(G, pos = pos, edges = G.edges(), width=edge_weights)

    else:
        networkx.draw_networkx_edges(G, pos = pos)

    networkx.draw_networkx_nodes(G, pos = pos, node_color=node_color_test, node_size=node_size_test)

    if(use_labels == True):

        labels = {}

        for node in G.nodes():
            labels[node] = node

        networkx.draw_networkx_labels(G, pos = pos, labels = labels)

    if(use_title):
        plt.title(title)

    plt.xticks([])
    plt.yticks([])

    plt.ylim([-0.5,scale+0.5])
    plt.xlim([-0.5,scale+0.5])

    plt.tight_layout()

    plt.savefig(filename)
    plt.close()
'''
def association_network(G, clusters, node_frequency, filename, node_scale = 0, edge_scale = 0, node_labels = True):
    
    import networkx
    import matplotlib.pyplot as plt

    max_edge_weight = 0
    max_node_weight = 0

    cluster_membership = set()

    if(node_frequency == {} or node_frequency == None):

        for node in G.nodes():
            node_frequency[node] = 1.0

    for edge in G.edges():

        cluster_membership.add(clusters[edge[0]])
        cluster_membership.add(clusters[edge[1]])

        if(G[edge[0]][edge[1]]['weight'] > max_edge_weight):
            max_edge_weight = G[edge[0]][edge[1]]['weight']

        if(node_frequency[edge[0]] > max_node_weight):
            max_node_weight = node_frequency[edge[0]]
        if(node_frequency[edge[1]] > max_node_weight):
            max_node_weight = node_frequency[edge[1]]

    color_map = get_cmap(len(cluster_membership))

    k = 2 / numpy.sqrt(len(G.nodes()))

    edge_weight = []

    if(node_scale > 0):
        max_node_weight = node_scale

    if(edge_scale > 0):
        max_edge_weight = edge_scale

    for edge in G.edges():

        weight = float(G[edge[0]][edge[1]]['weight'])/float(max_edge_weight)

        edge_weight.append((0,0,0,weight**1.15))

    node_colors = []
    node_weights = []

    for node in G.nodes():

        cluster_id = clusters[node]
        color = color_map(cluster_id)
        node_colors.append(color)
        node_weights.append(float(node_frequency[node])/float(max_node_weight) * 300.0 + 50.0)

    scale = 10

    pos = networkx.spring_layout(G, k=k, scale = scale)

    labels = {}

    for node in G.nodes():
        labels[node] = node

    networkx.draw_networkx_edges(G, pos = pos, width=2.5, edge_color = edge_weight)
    networkx.draw_networkx_nodes(G, pos = pos, node_size = node_weights, node_color = node_colors)

    if(node_labels):

        networkx.draw_networkx_labels(G, pos = pos, labels = labels)

    #plt.title(title)

    plt.xticks([])
    plt.yticks([])

    plt.ylim([-0.5,scale+0.5])
    plt.xlim([-0.5,scale+0.5])

    plt.tight_layout()

    plt.savefig(filename)
    plt.close()
'''
def stackedbar(value_keys, dict_array, xtick_labels, xlabel, ylabel, filename, rotation='horizontal', mapper = None, cmap = 'jet'):

    import matplotlib.pyplot as plt
    import matplotlib.patches as mpatches

    color_mapper = get_cmap(len(value_keys), cmap=cmap)

    if(mapper == None):
        mapper = {}

    try:

        fig, ax = plt.subplots()

        index = numpy.arange(len(xtick_labels))
        bar_width = 0.55

        opacity = 0.8
        error_config = {'ecolor': '0.3'}

        previous_top = []
        counter = 0

        patch_array = []

        for key in value_keys:

            data_array = []

            for data_dict in dict_array:

                #extract the data in the specified order
                data_array.append(data_dict[key])

            if(previous_top == []):
                previous_top = [0] * len(data_array)


            plt.bar(index, data_array, bar_width, alpha=opacity, color=color_mapper(counter), align='center', bottom=previous_top)

            #need to keep track of where we are on the plot.
            for i in range(0, len(data_array)):
                previous_top[i] = previous_top[i] + data_array[i]

            artist = mpatches.Patch(color = color_mapper(counter), label=mapper.get(key, key.upper()))
            patch_array.append(artist)
            
                    
            counter = counter + 1

        plt.axis('tight')

        plt.legend(handles = patch_array, loc='lower center', bbox_to_anchor=(0.5, -0.036*float(len(value_keys))), ncol = 3)

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        if(mapper == None):
            plt.xticks(index, xtick_labels, rotation = rotation)
        else:
            plt.xticks(index, [mapper.get(k,k) for k in xtick_labels] ,rotation=rotation)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')
    except:
        plt.close('all')
        raise

def generate_heatmap(matrix, names, ylabel, filename, vmin = None, vmax = None, mapper = None, use_labels=True, cmap='jet'):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()
        index = numpy.arange(len(names))

        if(vmin != None and vmax != None):
            plt.pcolormesh(matrix, vmin = vmin, vmax = vmax, cmap=cmap)
        else:
            plt.pcolormesh(matrix, cmap = cmap)

        cb = plt.colorbar()

        plt.axis('tight')
        
        #ax.set_ylabel(names)
        #ax.set_xlabel(names)

        #plt.xlabel(names)

        if(use_labels):

            if(mapper != None):
                names = [mapper.get(name, name) for name in names]
            else:
                names = list(names)

            plt.xticks(index+0.5, names, rotation='vertical')
            plt.yticks(index+0.5, names, rotation='horizontal')

            #ax.twinx().set_ylabel(ylabel)
            cb.set_label(ylabel)

            #plt.xlabel(index, names, rotation='vertical')
            ax.set_yticklabels(names, rotation='horizontal', va='center')
        else:
            #plt.xticks(index+0.5, [], rotation='vertical')
            #plt.yticks(index+0.5, [], rotation='horizontal')
            cb.set_label(ylabel)
            ax.set_yticklabels([], rotation='horizontal', va='center')
            ax.set_xticklabels([], rotation='vertical')

        
        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:

        plt.close('all')
        raise

def lineplot(xvalues, yvalues, legends,  xlabel, ylabel, filename, mapper=None, yerrors = None, ymin = 0, xmin = 0, showx = True):

    #yvalues is a list of lists, legends = len(yvalues), xvalues = len(yvalues[i])
    #if provided, yerrors also == len(yvalues)

    import matplotlib.pyplot as plt

    colors = get_cmap(len(yvalues))

    try:

        fig, ax = plt.subplots()

        y_max = 0

        frame1 = plt.gca()

        if(yerrors == None):
            for y,i in zip(yvalues, range(0,len(yvalues))):

                for v in y:
                    print v

                ax.plot(xvalues, y, color = colors(i), label = legends[i])
        else:
            for y,i in zip(yvalues, range(0,len(yvalues))):
                plt.errorbar(xvalues,y, yerr = yerrors[i], color = colors(i), label = legends[i])

                if(max(y) > y_max):
                    y_max = max(y)

        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)

        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels, loc='best')

        if(not showx ):
            frame1.axes.get_xaxis().set_visible(False)

        ax.set_ylim([ymin, y_max*1.1])

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:

        plt.close('all')
        raise

def linebar(keys, values, xvalues, yvalues, xlabel, y1label, y2label, filename, mapper=None, edgecolor='black', show_ticks = True):

    import matplotlib.pyplot as plt
    
    try:

        fig, ax = plt.subplots()

        index = numpy.arange(len(keys))
        bar_width = 0.7

        opacity = 0.8
        error_config = {'ecolor': '0.3'}

        ax.bar(index, values, bar_width,
                         alpha=opacity,
                         color='b',
                         align='center',
                        edgecolor = edgecolor)

        ax.set_ylabel(y1label)
        ax.set_xlabel(xlabel)

        ax.set_xticks(index)

        if(show_ticks == False):

            plt.tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom='off',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            labelbottom='off') # labels along the bottom edge are off
            
        ax2 = ax.twinx()
        ax2.plot(index, yvalues, 'r-', linewidth=2.0)
        ax2.set_ylabel(y2label, color='red')

        #turns off offset on graph if change between points is small
        ax2.get_yaxis().get_major_formatter().set_useOffset(False)

        ax2.yaxis.label.set_color('red')
        ax2.tick_params(axis='y', colors='red')


        if(mapper == None):
            ax.set_xticklabels([str(key) for key in keys], rotation = 'vertical')
        else:
            ax.set_xticklabels([str(mapper[k]) for k in keys], rotation = 'vertical')

        ax.set_ylim(0, max(values)*1.1)
        ax2.set_ylim(min(yvalues)*0.9, max(yvalues)*1.1)

        #plt.axis('tight')
        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def multi_bargraph(key_list, value_list, legend, xlabel, ylabel, filename, rotation='horizontal', mapper=None):

    import matplotlib.pyplot as plt

    color_order = ['c','r','y','g','b']

    try:

        fig, ax = plt.subplots()

        counter = 0

        width_bonus = 0.5

        lhandles = []

        for keys, values, color, lentry in zip(key_list, value_list, color_order, legend):

            index = numpy.arange(len(keys))
            bar_width = 0.35

            opacity = 0.8
            error_config = {'ecolor': '0.3'}

            print keys, values, 'printing out desired values'

            rects1 = plt.bar(index-float(bar_width)/2.0+bar_width*counter, values, bar_width,
                             alpha=opacity,
                             color=color,
                             align='center',
                             label = lentry)

            lhandles.append(rects1)

            counter +=1

        plt.legend(handles = lhandles, loc='best', ncol = 1)

        #plt.axis('tight')

        temp_ys = []

        for vlist in value_list:
            temp_ys.extend(vlist)

        #plt.ylim([0, max(temp_ys)*1.1])

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        if(mapper == None):
            plt.xticks(index, keys, rotation = rotation)
        else:
            plt.xticks(index, [mapper[k] for k in keys] ,rotation=rotation)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

        fhandle = open(filename+"_key labels.txt",'w')
        for key in keys:
            fhandle.write(key + "\n")
        fhandle.close()

    except:
        plt.close('all')
        raise
    
#here values is a iterable of lists
def multi_histogram(values, xlabel, ylabel, filename, rotation='horizontal',mapper=None,useMedianFilter=False, legends=None):

    import matplotlib.pyplot as plt
    import matplotlib.patches as mpatches

    colors = ['b','r','g','y']

    max_value = 0

    try:

        fig, ax = plt.subplots()

        first_bins = []

        patches = []

        for i in range(0, len(values)):

            if(not useMedianFilter):
                vrange = [min(values[i]), max(values[i])]
            else:
                vrange = [min(values[i]), 5.0*numpy.median(values[i])]

            if(i == 0):
                hist, first_bins = numpy.histogram(values[i], range=vrange)

            if(legends != None):
                patches.append(mpatches.Patch(color = colors[i], label=legends[i]))

            plt.hist(values[i], color=colors[i], bins = first_bins)

        plt.axis('tight')

        plt.legend(handles = patches, loc='upper right')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def cluster_plotting(data, idx, xlabel,ylabel, filename, rotation = 'horizontal',mapper=None):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()
        plt.plot(data[idx==0,0],data[idx==0,1],'ob', data[idx==1,0],data[idx==1,1],'or')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')
    except:
        plt.close('all')
        raise

def boxplot(xvalues, yvectors, xlabel, ylabel, filename, rotation='horizontal',mapper=None, showfliers=False):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()

        rects1 = plt.boxplot(yvectors, labels = xvalues, showfliers=showfliers)         

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def scatter(xvalues, yvalues, xlabel, ylabel, filename, rotation='horizontal',mapper=None, regression=None):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()

        rects1 = plt.scatter(xvalues, yvalues, color='b')

        if(regression != None):

            lsp = numpy.linspace(min(xvalues), max(xvalues))

            if(regression == 'linear'):
            
                slope, intercept, Rsq, pvalue, stderr = scipy.stats.linregress(xvalues, yvalues)

                reg_values = [slope * pos + intercept for pos in lsp]

                equation_str = '%0.3f*t + %0.2f' % (slope, intercept)

                equation_label = 'Linear'

            if(regression == 'exponential'):

                def test_func(x, a, b, c):
                    return a * numpy.exp(b * x) + c
                
                popt, pcov = curve_fit(test_func, numpy.asarray(xvalues), numpy.asarray(yvalues), p0 = [6, 0.3, 5])

                a = popt[0]
                b = popt[1]
                c = popt[2]

                if(c > 0):
                    equation_str = '%i exp(%0.3f*t) + %i' % (int(a),b,int(c))
                else:
                    equation_str = '%i exp(%0.3f*t) - %i' % (int(a),b,int(c)*-1)
                equation_label = 'Exponential'

                ybar = numpy.mean(yvalues)
                sstot = 0
                ssres = 0

                for x,y in zip(xvalues,yvalues):

                    sstot = sstot + (test_func(x, a, b, c) - ybar)**2
                    ssres = ssres + (test_func(x, a, b, c) - y)**2

                Rsq = 1 - ssres/sstot

                reg_values = [popt[0] * numpy.exp(popt[1] * x) + popt[2] for x in lsp]

            plt.annotate(equation_str, (0.05, 0.9), xycoords='axes fraction')
            plt.annotate('Rsq = {0:.3f}'.format(Rsq), (0.05, 0.85), xycoords='axes fraction')
            plt.plot(lsp, reg_values, color='r', label = equation_label, linewidth=3)
            

        plt.axis('tight')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def plotcdf(values, xlabel, ylabel, legend, filename, rotation='horizontal',mapper=None, plot_survival = True):

    import matplotlib.pyplot as plt

    data_container = []

    try:

        fig, ax = plt.subplots()


        d, base = numpy.histogram(values, bins = 50)
        cumulative = numpy.cumsum(d)

        plt.plot(base[:-1], cumulative, label=legend, linewidth=2.0)

        if(plot_survival):

            plt.plot(base[:-1], len(values) - cumulative, label='Survival (1-y)', linewidth=2.0)

        plt.axis('tight')

        handles, labels = ax.get_legend_handles_labels()

        ax.legend(handles, [legend, 'Survival (1-y)'], loc='best')

        plt.xlim([0, 100.1])

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def histogram(values, xlabel, ylabel, filename, rotation='horizontal',mapper=None,useMedianFilter=False):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()


        if(useMedianFilter):
            rects1 = plt.hist(values, color='b', range=[min(values), 5.0*numpy.median(values)])
        else:
            rects1 = plt.hist(values, color='b')

        plt.axis('tight')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def simple_bargraph(keys, values, xlabel, ylabel, filename, rotation='horizontal', mapper=None, yerr = None):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()

        index = numpy.arange(len(keys))
        bar_width = 0.55

        opacity = 0.8
        error_config = {'ecolor': '0.3'}

        rects1 = plt.bar(index, values, bar_width,
                         alpha=opacity,
                         color='b',
                         align='center')

        if(yerr != None):
            plt.errorbar(index, values, yerr=yerr, fmt ='none', color = 'k')

        plt.axis('tight')

        if(yerr != None):
            max_index = numpy.where( values == max(values) )
            plt.ylim([0, max(values)+yerr[max_index[0]]*1.2])

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        if(mapper == None):
            plt.xticks(index, keys, rotation = rotation)
        else:
            plt.xticks(index, [mapper.get(k,mapper.get(k.upper(),k)) for k in keys] ,rotation=rotation)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

        fhandle = open(filename+"_key labels.txt",'w')
        for key in keys:
            fhandle.write(str(key) + "\n")
        fhandle.close()

    except:
        plt.close('all')
        raise
