__author__ = 'jdwinkler'

from collections import defaultdict
import Standardization
from Standardization import Standard
import DatabaseUtils
import GraphUtils as glib
import os
import networkx
import NetworkRepresentation
import numpy
import sys
import gene_ontology

'''

refactoring to do

one: break down network analysis into whole association network
two: individual product analysis
three: cluster systems biology analyis using GO approach
four: proposed chassis designs
five: recommendation system
six: design rules inferred from systems biology analysis

'''

def associative_to_nxgraph(associative_data):

    G = networkx.Graph()

    edges = []

    observed = set()

    #create adj list
    for node_x in associative_data:
        for node_y in associative_data[node_x]:

            if((node_x, node_y) in observed or (node_y, node_x) in observed):
                continue

            observed.add((node_x,node_y))
            observed.add((node_y,node_x))

            edges.append((node_x,node_y, {'weight':associative_data[node_x][node_y]}))

    G.add_edges_from(edges)

    return G

def filter_designs(category_filter, laser_database_papers, metabolite_standard_dict, product_ontology_dict, standard, target_species, aggregate = True):

    if(type(category_filter) != list):
        category_filter = list(category_filter)

    products_in_filter = Standardization.generate_product_dicts(category_filter, metabolite_standard_dict, product_ontology_dict)

    flattened = []

    for category_set in products_in_filter:
        flattened.extend(category_set)

    flattened = set(flattened)

    design_info = Standardization.get_designs_by_product(flattened, metabolite_standard_dict, laser_database_papers, standard, species=target_species)

    return design_info

def generate_network(filtered_designs):

    nx_association_network, node_frequencies = NetworkRepresentation.generate_assocation_network(filtered_designs)

    return nx_association_network, node_frequencies

def apply_clustering(nx_association_network, clustering_method):

    nodes_to_clusters, clusters_to_nodes = DatabaseUtils.compute_igraph_clustering(nx_association_network,clustering_method)

    return nodes_to_clusters, clusters_to_nodes

def network_to_chassis_designs(nx_association_network, node_cluster_membership, number_of_designs):

    chassis_strain_candidates = NetworkRepresentation.identify_graph_chassis(nx_association_network,node_cluster_membership,number_of_designs,percentile=99)

    return chassis_strain_candidates

def draw_association_network(nx_association_network, node_frequencies, clusters, filename):

    #outputs colored, weighted graph for paper

    glib.association_network(nx_association_network, clusters, node_frequencies, filename)

def build_GO_datasets(species_source, go_source, data_key):

    GO_dict, GO_network = gene_ontology.parse_GO_file(go_source,data_key)

    species_dict = gene_ontology.load_species_dataset(species_source)

    return GO_dict, species_dict, GO_network

def network_analysis_helper(title, filtered_designs, species):

    #print 'Processing submitted designs for title: %s' % title
    #print 'Number of designs = %i' % len(filtered_designs)

    #goals: cluster networks, output cluster identities, export graphs,

    nx_association_network, node_frequencies = generate_network(filtered_designs)

    node_by_clusters, clusters_by_node = apply_clustering(nx_association_network, 'fastgreedy')

    #chassis_designs = network_to_chassis_designs(nx_association_network, node_by_clusters, len(filtered_designs))

    cluster_output_file = os.path.join(DatabaseUtils.OUTPUT_DIR, 'rules_paper',title + '_' + species + '_clustering_report.txt')

    fhandle = open(cluster_output_file,'w')

    for cluster_id in clusters_by_node:

        fhandle.write('\t'.join([str(cluster_id), ','.join(clusters_by_node[cluster_id])]) + '\n')
    fhandle.close()

    #todo: generate mutational spectra for each mutation


    #generate annotated figures for discussion.

    network_figure_output_file = os.path.join(DatabaseUtils.OUTPUT_DIR, 'rules_paper',title + '_' + species + '_fastygreedy_clustered_network.pdf')

    glib.association_network(nx_association_network,node_by_clusters,node_by_clusters,network_figure_output_file, node_labels=False)

    #print 'Finished processing (%s, %s) basic network evaluation data' % (species, title)

    return None

def gene_association(categories, data_dict, threshold = 200):

    #todo: maybe make this less of an atrocity against coding, but it seems to work well enough so long as N(genes)/N(conditions) is low

    association_matrix = defaultdict(dict)

    design_total = 0

    for cat in categories:
        for (mutation_dict, _, _) in data_dict[cat]:
            design_total+=1
            for i in mutation_dict:
                for j in mutation_dict:
                    if(i == j):
                        association_matrix[i][j] = 0

                    if(j not in association_matrix[i]):
                        association_matrix[i][j] = 1
                    else:
                        association_matrix[i][j] +=1

    keys_to_remove = set()

    for key_x in association_matrix:
        total_interactions = 0
        for key_y in association_matrix[key_x]:
            total_interactions = total_interactions + association_matrix[key_x][key_y]

        if(total_interactions < threshold):
            keys_to_remove.add(key_x)

    print keys_to_remove

    #sys.exit(0)


    rebuilt_association_matrix = defaultdict(dict)

    for key_x in association_matrix:

        if(key_x in keys_to_remove):
            continue
        for key_y in association_matrix[key_x]:
            if (key_y in keys_to_remove):
                continue
            rebuilt_association_matrix[key_x][key_y] = association_matrix[key_x][key_y]

    association_matrix = rebuilt_association_matrix

    #normalize
    for gene_x in association_matrix:
        for gene_y in association_matrix[gene_x]:
            association_matrix[gene_x][gene_y] = float(association_matrix[gene_x][gene_y])/float(design_total)*100.0

    return association_matrix

def build_matrix(category_association_matrix):

    rows = category_association_matrix.keys()

    data = []
    for condition_x in rows:

        columns = []
        #symmetrical matrix
        for condition_y in rows:

            if(condition_y in category_association_matrix[condition_x]):
                columns.append(category_association_matrix[condition_x][condition_y])
            else:
                columns.append(0)

        data.append(columns)

    return numpy.matrix(data), rows


def category_correlation_analysis(title, category_design_dict, species, threshold = 5):


    #overlap in manipulated genes based on product category.
    #will also compute correlation coefficients between categories (maybe use that for the figure instead)

    #note that category design dict is a dict of arrays of dicts. !!!

    rows_to_process = []

    construct_bipartite_network(category_design_dict)

    for key in category_design_dict:

        if(len(category_design_dict[key]) > threshold):
            rows_to_process.append(key)

    output_file_name = os.path.join(DatabaseUtils.OUTPUT_DIR,species+'_category_'+title +'.pdf')

    gene_association_matrix = gene_association(rows_to_process,category_design_dict)

    gene_network = associative_to_nxgraph(gene_association_matrix)

    node_to_clusters, clusters_to_node = apply_clustering(gene_network,'multilevel')

    #glib.association_network(gene_network, node_to_clusters, {}, os.path.join(DatabaseUtils.OUTPUT_DIR,'Crude Association Network.pdf'),node_labels=False)

    #gene_matrix, gene_rows = build_matrix(gene_association_matrix)

    #glib.generate_heatmap(numpy.array(gene_matrix), [r.upper() for r in gene_rows], 'Overlap Percentage (%)', output_file_name.replace('_category_','_gene_'), use_labels=False)

def generate_go_report(title, GO_dict, species_dict, GO_network, go_to_gene, number_of_genes, counting_stats, random_seed, threshold = 1e-6):

    #todo: add seed to function for reproducibility

    p_values = gene_ontology.compute_enrichment(species_dict, number_of_genes,counting_stats, seed = random_seed)

    output_file = []

    nodes_to_retain = set()

    for key in p_values:
        if(p_values[key] < threshold):
            output_file.append((key, p_values[key], GO_dict[key]['name'], ','.join(go_to_gene[key])))
            nodes_to_retain.add(key)
            #print key, p_values[key], GO_dict[key]['name'], ','.join(go_to_gene[key])

    fhandle = open(os.path.join(DatabaseUtils.OUTPUT_DIR,title+'_GO_statistics.txt'),'w')

    for line in output_file:

        line = [str(x) for x in line]

        fhandle.write('\t'.join(line) + '\n')
    fhandle.close()

    return p_values

def gene_ontology_analysis(title, filtered_designs, species, go_source_data = 'go-basic.obo', threshold=1e-6):

    if(species.upper() == 'Escherichia coli'.upper()):
        species_path = 'gene_association.ecocyc'
    if(species.upper() == 'Saccharomyces cerevisiae'.upper()):
        species_path = 'gene_association.sgd'

    if(species_path == None):
        raise AssertionError('You need to provide a known species type to load gene ontology data: %s is not valid.' % species)

    #GO_dict analysis is done in the gene ontology class
    #species dict is gene-list of keys I think
    GO_dict, species_dict, GO_network = build_GO_datasets(os.path.join(DatabaseUtils.INPUT_DIR,species_path),os.path.join(DatabaseUtils.INPUT_DIR,go_source_data),'name')

    design_go_list = []

    go_to_gene = defaultdict(set)

    for (design_dict, doi, mutant) in filtered_designs:
        for gene in design_dict:
            if(gene.upper() in species_dict):
                design_go_list.append((gene.upper(), species_dict[gene.upper()]))

                for go in species_dict[gene.upper()]:
                    go_to_gene[go].add(gene.upper())

    #print len(design_go_list), len(set([x[0] for x in design_go_list]))

    _, counting_stats = gene_ontology.gene_set_statistics(GO_dict,design_go_list,'name')

    return generate_go_report(title,GO_dict,species_dict,GO_network,go_to_gene,len(design_go_list),counting_stats,50,threshold=threshold)

    #for key in counting_stats:
    #    print key, counting_stats[key], GO_dict[key]['name']


def generate_analysis(species):

    papers = DatabaseUtils.getDatabase()

    correlation_analysis_dataset = {}

    correlation_holder = []

    for paper in papers:
        for mutant in paper.mutants:

            mutant_dict = {}

            for gene in mutant.mutations:
                mutant_dict[gene.name] = (gene.changes, gene.source)

            correlation_holder.append((mutant_dict, paper.doi, mutant))

    correlation_analysis_dataset['all'] = correlation_holder

    category_correlation_analysis('correlation analysis', correlation_analysis_dataset, species)

    gene_ontology_analysis('Gene Ontology Resistance Analysis',correlation_holder,'Escherichia coli')


#chassis strain output function, needs to provide more statisics regarding the strain
if(__name__ == '__main__'):

    #new idea-add an additional type of mutation "add" that activates heterologous reactions that nonetheless use entirely native metabolites
    #e.g. cimA
    #might open up novel metabolic pathways that cannot be considered in traditional genetic algorithms
    '''

    #generate_performance_data()

    generate_discussion_data()

    targets,_ = build_target_rxns('iJO1366')

    product_order = targets.keys()

    files = []

    for key in product_order:

        #files.append(os.path.join(DatabaseUtils.OUTPUT_DIR,'simulator data',key + '_True_flux_data.pkl'))
        files.append(os.path.join(DatabaseUtils.OUTPUT_DIR,'simulator data',key + '_False_flux_data.pkl'))

    print files

    process_performance_data(files, product_order)

    #mutation_tuples = generate_chassis('alcohols')

    '''

    generate_analysis('Escherichia coli')

    #test_functionality()

#todo: you can represent phenotype-gene relationships as a bipartite graph





