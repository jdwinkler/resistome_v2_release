__author__ = 'keerickson'

import DatabaseUtils

papers = DatabaseUtils.getDatabase()

output_file = set()

for paper in papers:

    #output_file.add((paper.title,paper.doi,str(paper.year)))
    for mutant in paper.mutants:
        for mutation in mutant.mutations:
            output = (mutation.name + '\t' + mutation.changes[0] + '\t' + paper.title + '\n')
            output_file.add(output)

fhandle = open('Resistome_all_papers_mutants.txt','w')

for line in output_file:
    fhandle.write(line)
fhandle.close()
