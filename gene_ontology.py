__author__ = 'jdwinkler'

import networkx
import os
import numpy
import scipy.stats
import DatabaseUtils
from collections import defaultdict
import sys

def process_GO_buffer(buffer, keys):

    closure = defaultdict(list)

    keys = set(keys)

    keys.add('id')
    keys.add('is_a')

    id_key = ''

    for line in buffer:

        tokens = [x.strip() for x in line.split(' ',1)]

        key = tokens[0].replace(':','')
        value = tokens[1]

        #print line, tokens, key, value

        if(key not in keys):
            #not of interest, skip line
            continue

        if(key == 'id'):
            id_key = value
        else:
            closure[key].append(value)

    edges = []

    #this handles the case where the term is a root of a graph (has no is_a)
    if('is_a' in closure):

        for parent in closure['is_a']:

            parent = parent.split(' ! ')[0].strip()

            edges.append((parent, id_key))

    for key in closure:

        #if(len(closure) == 1):
        closure[key] = closure[key][0]

    return id_key, closure, edges

def parse_GO_file(file_name, keys_of_interest):

    #converts GO file into a queryable python dictionary
    #you can also pass the output to build_SQL_database to do that (if you want, I may not)

    GO_term = []
    add = False

    GO_dict = {}

    if(type(keys_of_interest) != list):
        keys_of_interest = [keys_of_interest]


    GO_network = networkx.DiGraph()

    #todo: output relational graph of GO terms as well

    for line in open(file_name,'rU'):

        line = line.strip()

        if(len(line) == 0 or line[0] == '#'):
            #comment, continue (I had to manually add this)

            #line break
            if(len(line) == 0 and add == True):
                #represents a break between GO terms
                id, record, edges = process_GO_buffer(GO_term, keys_of_interest)

                GO_network.add_edges_from(edges)

                GO_term = []
                GO_dict[id] = record
                add = False

            continue

        elif(add):

            GO_term.append(line)

        if('[term]' in line.lower()):
            add = True

    if(len(GO_dict.keys()) == 0):
        raise AssertionError('Zero size go dict?')

    return GO_dict, GO_network

def gene_set_statistics(GO_dict, gene_go_list, keys_to_analyze):

    counting_statistics = defaultdict(int)

    output_dict = {}

    #single key of interest
    if(type(keys_to_analyze) != list):
        keys_to_analyze = [keys_to_analyze]

    for (gene, go_list) in gene_go_list:

        data = {}

        for go in go_list:
            for key in keys_to_analyze:
                data[go] = GO_dict[go][key]

                counting_statistics[go] = counting_statistics[go] + 1

        output_dict[gene] = data


    return output_dict, counting_statistics

#computes the statistical significance of GO frequency in the counting stats dict
#implements a simple monte-carlo boostraping algorithm: (from wikipedia)

#The Monte Carlo algorithm for case resampling is quite simple. First, we resample the data with replacement, and the size of the resample must be equal to the size of the original data set.
#Then the statistic of interest is computed from the resample from the first step. We repeat this routine many times to get a more precise estimate of the Bootstrap distribution of the statistic.
def compute_enrichment(species_GO_dict, dataset_size, counting_stats, trials = 1000, apply_bonferroni = True, seed = 50):

    def dict_frequency(data_dict):

        total_tags = 0
        for key in data_dict:
            total_tags += data_dict[key]

        frequency_dict = {}

        for key in data_dict:
            frequency_dict[key] = float(data_dict[key])/float(total_tags)

        return frequency_dict

    numpy.random.seed(seed)


    #species/target go dicts are gene-go codes, counting stats is a go-int dictionary
    result_stats = defaultdict(list)
    counting_stats = dict_frequency(counting_stats)
    for i in range(0, trials):
        #select number of genes equal to number of keys in the target_go_dict, with replacement, without weights
        random_genes = numpy.random.choice(species_GO_dict.keys(), size = dataset_size, replace=True)

        temp_counting_stats = defaultdict(int)

        for gene in random_genes:
            for go in species_GO_dict[gene]:
                temp_counting_stats[go] += 1

        #trial_results.append(temp_counting_stats)

        #print temp_counting_stats

        frequency_dict = dict_frequency(temp_counting_stats)

        for key in frequency_dict:
            result_stats[key].append(frequency_dict[key])

    #have all data, want to compute probability of observing counting_stats (from target GO dict) in distribution

    collated_dict = {}
    p_values = {}
    comparison_count = 0
    no_variation = set()

    for key in result_stats:

        trial_mean = numpy.mean(result_stats[key])
        trial_std  = numpy.std(result_stats[key])

        collated_dict[key] = (trial_mean, trial_std)

        if(trial_std == 0):
            no_variation.add(key)
            #print 'No variation detected for GO ID %s in %i samples' % (key, trials)
            continue

        if(key in counting_stats):
            p_values[key] = scipy.stats.norm.cdf((trial_mean - counting_stats[key])/trial_std)
            comparison_count+=1
        else:
            p_values[key] = scipy.stats.norm.cdf((trial_mean - 0)/trial_std)
            comparison_count+=1

    if(apply_bonferroni and comparison_count != 0):

        alpha_modifier = 1.0/float(comparison_count)

        for key in p_values:
            p_values[key] = p_values[key]*alpha_modifier

    return p_values

def load_eggnog_functional_annotations(functional_table, standard_obj, species):

    gene_to_pathway = {}

    for line in open(functional_table,'rU'):

        if(line[0] == '#'):
            continue

        tokens = line.strip().split('\t')

        gene_name = standard_obj.convert(tokens[0],species)

        if(len(tokens) > 1):
            cogs = tokens[1].split(',')
        else:
            cogs = []

        gene_to_pathway[gene_name.upper()] = cogs

    return gene_to_pathway

'''

algorithm description

given two nodes in directed G, find a node (not necessarily all nodes) that is closest both node1 and node2.

essentially, the algorithm will keep pushing the predecessors

'''
def ancestor_search(network, node1, node2):

    node1_search = [node1]
    node2_search = [node2]

    #found lca (yourself)
    if(node1 == node2):
        return 0

    level = 1

    #infer list of roots by finding nodes with only descendants (no predecessors)
    #note this only works if the network is acylical (otherwise you'll get an endless loop)

    #roots = set()

    #for node in network.nodes():
    #    if(len(network.predecessors(node)) == 0):
    #        roots.add(node)

    node1_levels = defaultdict(int)
    node2_levels = defaultdict(int)

    while(True):

        #todo: keep track of vertex levels to return the minimum lca, right now this walks back too much

        node1_set = set()
        node2_set = set()

        for node in node1_search:
            node1_levels[node] = level
            node1_set.update(network.predecessors(node))

        for node in node2_search:
            node2_levels[node] = level
            node2_set.update(network.predecessors(node))

        #no more nodes to process
        if(len(node1_set) == 0 or len(node2_set) == 0):
            break

        #lca discovered (somewhere)
        #if(not node1_set.isdisjoint(node2_set)):
        #    break

        node1_search = node1_set
        node2_search = node2_set

        level+=1

    #check the dictionaries for the vertex that has the lowest level in both dictionaries

    found_nodes = set()
    found_nodes.update(set(node1_levels.keys()) & set(node2_levels.keys()))

    minimum_level = -1
    first = True

    avg_function = lambda x,y: 0.5*float(x) + 0.5*float(y)

    for node in found_nodes:

        if(first):
            minimum_level = avg_function(node1_levels[node], node2_levels[node])
            first = False
        elif(avg_function(node1_levels[node], node2_levels[node]) < minimum_level):
            minimum_level = avg_function(node1_levels[node], node2_levels[node])

    #print minimum_node, minimum_level

    return minimum_level


#finds a common ancestor
def lowest_common_ancestor(GO_network, root, node1, node2):

    shortest_paths = []

    nodes = [node1, node2]

    longest_sp = 0

    for node in nodes:

        #skip it, no connecting path
        if(not networkx.has_path(GO_network, root, node)):
            break

        #has a connecting path
        shortest_paths.append(networkx.shortest_path(GO_network, source=root, target=node))

    if(len(shortest_paths) != 2):

        return (None, -1, -1)

    path_x = shortest_paths[0]
    path_y = shortest_paths[1]

    for i,j in zip(range(0, len(path_x)), range(0, len(path_y))):
        if(path_x[i] != path_y[j]):
            break

    #print path_x[i-1], float(i)/float(len(path_x))

    if(path_x[i - 1] != root):

        print path_x, 'x'
        print path_y, 'y'

        print 'lowest common ancestor: %s' % path_x[i-1]


    return path_x[i-1], float(i-1)/float(len(path_x)), float(j-1)/float(len(path_y))

def most_specific_tags_only(species_dataset, GO_network, root_process = 'GO:0008150', predecessor_level = 0, minimum_root_distance = 0):

    reduced_dict = {}

    #biological process, cellular component, molecular function
    roots = ['GO:0008150', 'GO:0005575', 'GO:0003674']

    if(root_process == None):
        print 'Must select a root node to compare tag depths.'
        print 'biological_process, cellular component, and molecular function',roots
        raise AssertionError('Invalid root node provided.')

    for key in species_dataset:

        go_tags = species_dataset[key]

        longest_root_route = 0
        specific_tag = ''

        for tag in go_tags:

            if(not networkx.has_path(GO_network, root_process, tag)):
                continue

            shortest_path_length = len(networkx.shortest_path(GO_network,source=root_process,target=tag))

            if(shortest_path_length > longest_root_route):
                specific_tag = tag
                longest_root_route = shortest_path_length

        #found the most specific tag.

        #note: empty list if no path between root/target node.

        if(specific_tag == ''):
            continue

        output = [specific_tag]

        for i in range(0, predecessor_level):

            temp_output = set()

            for tag in output:
                temp_output.update(GO_network.predecessors(tag))

            if(root_process in temp_output):
                temp_output.remove(root_process)

            tags_to_remove = set()

            for tag in temp_output:

                #+2 for source_tag
                if(len(networkx.shortest_path(GO_network,source=root_process,target=tag)) < minimum_root_distance + 2):
                    tags_to_remove.add(tag)

            for tag in tags_to_remove:
                temp_output.remove(tag)

            #no more predecessors to remove
            if(len(temp_output) == 0 and len(output) > 0):
                break

            output = temp_output

        reduced_dict[key] = output

    return reduced_dict

def load_species_dataset(go_file, standard_obj, species):

    output_dict = defaultdict(set)

    for line in open(go_file,'rU'):

        if('!' == line[0]):
            continue

        tokens = line.split('\t')

        gene = standard_obj.convert(tokens[2].strip(), species)

        go_id = tokens[4].strip()

        output_dict[gene.upper()].add(go_id)
        output_dict[gene].add(go_id)

    return output_dict

def test_functionality():

    input_dir = DatabaseUtils.INPUT_DIR

    ecoli_sample_data = load_species_dataset(os.path.join(input_dir,'gene_association.ecocyc'))

    keys = ['id', 'name', 'namespace','def']

    GO_dict, GO_network = parse_GO_file(os.path.join(input_dir,'go-basic.obo'), keys)

    subset = GO_dict.keys()

    test_go_sample = ['GO:0003756', 'GO:0008270', 'GO:0006457', 'GO:0042026', 'GO:0051082', 'GO:0016020', 'GO:0005737', 'GO:0008270', 'GO:0016020', 'GO:0008270','GO:0006260','GO:0009408']

    synthetic_dict = {'dnaJ':test_go_sample}

    output_dict, counts = gene_set_statistics(GO_dict, ecoli_sample_data, 'name')

    for key in counts:
        print key, counts[key]

if(__name__ == '__main__'):
    test_functionality()






