import DatabaseUtils
import os
from Standardization import Standard

papers = DatabaseUtils.getDatabase(Standard('',empty=True),Standard('',empty=True),years=('2015',))

output_dir = os.path.join(DatabaseUtils.DATABASE_DIR, 'refactored')

numMK = 'NumberofMutants'
numGK = 'NumberofMutations'


for paper in papers:

    altered = False

    mutants = int(paper.paperBacking[numMK])


    for i in range(1, mutants + 1):

        mutations = int(paper.mutantBacking[(i, numGK)])

        for j in range(1, mutations + 1):

            changes  = paper.geneBacking[(i,j,'GeneMutation')]

            for change in changes:

                if('aa_snps' in change and 'STOP' in paper.annotationBacking[(i,j,'GeneMutation',change)].upper()):
                    paper.annotationBacking[(i,j,'GeneMutation',change)] = paper.annotationBacking[(i,j,'GeneMutation',change)].upper().replace('STOP','*')
                    altered = True

    if(altered):
        paper.rewrite(output_dir)
