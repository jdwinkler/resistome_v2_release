__author__ = 'jdwinkler'

from itertools import cycle
import DatabaseUtils
from Standardization import Standard
import Standardization
import NetworkRepresentation
import networkx
import os

def output_circos_linkage_annotatons(network, included_genes, location_data, output_file, exclusions = set(), weight_threshold = 5, weight_bonus = 0, write_file = True):

    output = []

    modified_nodes = included_genes & set(network.nodes())

    missing_set = set()

    for node in modified_nodes:

        neighbors = set(network.neighbors(node)) & included_genes

        if(node in exclusions):
            continue

        if(node not in missing_set):
            missing_set.add(node)

        for neighbor in neighbors:

            if(neighbor in exclusions):
                continue

            if(neighbor not in location_data):
                missing_set.add(neighbor)

            #print G[node][neighbor]['weight']

            weight = network[node][neighbor]['weight']

            if(weight < weight_threshold):
                continue

            if(node not in location_data):
                print 'Gene missing from location data: %s' % (node)
                print 'Skipping...'
                continue

            if(neighbor not in location_data):
                print 'Neighboring gene missing %s' % neighbor
                print 'Skipping'
                continue


            (chr_s, start_s, end_s) = location_data[node]
            (chr_d, start_d, end_d) = location_data[neighbor]

            output_string = chr_s + '\t' + '\t'.join([str(start_s), str(end_s)])  + '\t' + chr_d + '\t' + '\t'.join([str(start_d), str(end_d)]) +'\tthickness=' + str(weight + weight_bonus)
            output.append(output_string)

    if(write_file):
        fhandle = open(output_file, 'w')
        for line in output:
            fhandle.write(line + '\n')
        fhandle.close()

    return output

def output_circos_labels(target_genes, location_data, output_name, write_file = True):

    output = []

    for tup in target_genes:

        (gene, height) = tup

        (chromosome, start, end) = location_data[gene]

        output_string = chromosome + '\t' + str(start) + '\t' + str(end) + '\t' + gene# + '\tr0=2.0r,r1=3.0r'
        output.append(output_string)

    if(write_file):
        fhandle = open (output_name, 'w')
        for line in output:
            fhandle.write(line + '\n')
        fhandle.close()

    return output

def output_circos_gene_annotations(included_genes, location_data, node_frequency, output_name, r0 = 1.0, label_threshold=20, write_file = True):

    import numpy

    colors = ['green']

    #cyclical_colors = cycle(colors)

    output = []

    modified_genes = set(location_data.keys()) & included_genes

    min_height = 10
    height_median = numpy.median(node_frequency.values())

    labeled_peaks = []

    for gene in modified_genes:

        if(gene not in location_data):
            print 'Gene missing: %s' % gene
            continue

        (chromosome, start, end) = location_data[gene]

        median_ratio = float(node_frequency[gene]) / height_median

        height = min_height * median_ratio

        if(median_ratio > label_threshold):
            labeled_peaks.append((gene, height))

        #+ '\tfill_color=' + cyclical_colors.next()
        output_string = chromosome + '\t' + str(start) + '\t' + str(end) + '\t' + gene + '\tr0=' + str(r0)+'r,r1=' + str(r0) + 'r+' + str(height) + 'p'

        output.append(output_string)

    if(write_file):
        fhandle = open(output_name,'w')
        for line in output:
            fhandle.write(line + '\n')
        fhandle.close()

    labels = output_circos_labels(labeled_peaks, location_data, output_name + '_labels.txt', write_file = write_file)

    return output, labels

def load_genome(species, filename, chromosome, filter):

    fhandle = open(filename,'rU')
    lines = fhandle.readlines()
    fhandle.close()

    location_dict = {}

    offset = 1

    if('proteins' in lines[offset]):
        offset = 3

    for line in lines[offset:]:

        tokens = line.strip().split('\t')

        length = tokens[0].split('..')

        start = int(length[0])
        end   = int(length[1])
        gene  = tokens[4]

        if(gene == '-'):
            gene = tokens[5]

        if(start > end):
            temp = start
            start = end
            end = temp

        gene = gene.upper()

        location_dict[filter.convert(gene, species.upper()).upper()] = (chromosome, start, end)
        #location_dict[gene.upper()] = (chromosome, start, end)

    return location_dict

def get_native_genes(papers, standard, target_species = 'Escherichia coli',exclusions = set()):

    included_genes = set()

    for paper in papers:

        for mutant in paper.mutants:

            if(mutant.species.upper() != target_species.upper()):
                continue

            for gene in mutant.mutations:

                name = gene.name.upper()
                source = gene.source.upper()

                if(name in exclusions):
                    continue

                if(len(name) < 2):

                    print name
                    print paper.title
#                    raise AssertionError

                if('CLS' == name.upper()):
                    print name
                    print paper.title
#                    raise AssertionError

                if(source == target_species.upper()):
                    included_genes.add(standard.convert(name, source).upper())

    return included_genes

def get_graph(papers, standard_object, species):

    from collections import defaultdict

    design_info = Standardization.get_designs_by_product([], {}, papers, standard_object, species = species, ignore_categories=True)

    counter = defaultdict(int)

    for (design, doi, m_info) in design_info:
        for gene in design.keys():
            counter[gene.upper()] = counter[gene.upper()] + 1
            (mutation_list, source) = design[gene]
            if(source.upper() != species.upper()):
                del design[gene]

    G, node_freq = NetworkRepresentation.generate_assocation_network(design_info)

    return G, node_freq

def generate_ecoli_config(papers):

    standard = Standard(None, empty=True)

    species = 'Escherichia coli'

    included_genes = get_native_genes(papers, standard, target_species=species)

    G, node_frequency = get_graph(papers, standard, species)

    location_data = load_genome(species.upper(), DatabaseUtils.INPUT_DIR+'ecoli_genome_annotations.txt', 'chr1', standard)

    output_circos_gene_annotations(included_genes,location_data,node_frequency,os.path.join(os.getcwd(),'circos','bin','ecgenes_test.txt'), r0 = 1, label_threshold=10)

    output_circos_linkage_annotatons(G, included_genes, location_data, os.path.join(os.getcwd(),'circos','bin','ecgenes_links.txt'))


papers = DatabaseUtils.getDatabase()

#generate_yeast_config(papers)

generate_ecoli_config(papers)



