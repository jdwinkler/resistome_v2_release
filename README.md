# README #

Welcome to the repository for the Tolerome - a database designed to provide insight into transcriptomic basis for *E. coli* resistance and tolerance phenotypes. This database contains more than 56,000 gene expression changes recorded to 89 different stress conditions. Please refer to the associated publication (Erickson et. al. 2017) to learn more. 

### How do I get set up? ###

**What do I need?**
Software for loading and analyzing the database is written in Python 2.7, which can be installed from [here](https://www.python.org/download/releases/2.7/). Many subpackages are handy for performing analysis, including scipy, pandas, and statsmodels. We recommend installing subpackages via the Anaconda distribution, available [here](https://www.continuum.io/downloads).

**Where are the records?**
Database files are flat, key-value text files designed for straightforward parsing. The list of allowed terms and their corresponding types are given in the InputFields_typed.txt file under ../inputs/. Records compatible with the Tolerome (i.e. containing gene expression data) are stored under ../database_store/2017_ge. Standardized terms for the values in the records are specified in the ../inputs/Term Usage.txt file.

**Parsing the Tolerome.**
Records are parsed using the Paper, Mutant, and Transcriptome classes within MetEngDatabase. DatabaseUtils.getDatabase method will return an array of Paper objects containing strains with gene expression changes, allowing you to interrogate each strain/paper as desired. Gene names and stress conditions are automatically standardized using a many to one mapping; gene names are specified in ../inputs/Species-Specific Gene Names.txt, and metabolites in ../inputs/Compound Information.txt. A simple ontology describing the relationship of specific stressors to overarching classes (thermal stress, solvent stress, etc) is included in ../inputs/Stress Classifications.txt.

**Can I see an example?**
The script GE_probe.py demonstrates how to load and output data from the database. You can also check out GE_functions.py for examples on the sorts of analysis this database enables.

### Contribution guidelines ###

If you would like to contribute data or code, please write us at the email addresses below.

* Keesha E. Erickson (keesha.e@gmail.com)
* James D. Winkler (james.winkler@gmail.com)
* Anushree Chatterjee (chatterjee@colorado.edu)

### License ###

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE