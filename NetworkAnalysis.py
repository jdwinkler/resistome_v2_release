__author__ = 'jdwinkler'

import networkx
from Standardization import Standard
import DatabaseUtils
import gene_ontology
from collections import defaultdict
import itertools


def generate_genetic_interaction_network(papers, minimum_weight = 0, conversion_dict = None):

    edges = defaultdict(dict)

    for paper in papers:
        for mutant in paper.mutants:

            interaction_pairs = set()

            gene_names = [mutation.name.upper() for mutation in mutant.mutations]

            if(conversion_dict != None):

                converted_info = []

                for gene in gene_names:

                    if(gene in conversion_dict):
                        converted_info.extend(conversion_dict[gene])
                    else:
                        continue

                gene_names = set(converted_info)

            pairwise_combinations = itertools.combinations(gene_names,2)

            for (gene1, gene2) in pairwise_combinations:

                if(gene1 == gene2):
                    continue

                if((gene1, gene2) in interaction_pairs or (gene2, gene1) in interaction_pairs):
                    continue

                interaction_pairs.add((gene1, gene2))

            for (gene_x, gene_y) in interaction_pairs:

                if(gene_y in edges[gene_x]):
                    edges[gene_x][gene_y]+=1
                else:
                    edges[gene_x][gene_y]=1

    edge_tuples = []

    for gene_x in edges:
        for gene_y in edges[gene_x]:

            if(edges[gene_x][gene_y] > minimum_weight):
                edge_tuples.append((gene_x, gene_y, {'weight': edges[gene_x][gene_y]}))

    G = networkx.Graph()
    G.add_edges_from(edge_tuples)

    return G

def generate_bipartite_gene_phenotype_network(papers, stress_ontology, use_switch = 'tolerant', minimum_weight = 0, conversion_dict = None):

    allowed_switches = {'tolerant', 'sensitive', 'all'}

    if(use_switch not in allowed_switches):
        raise AssertionError('Invalid network subset (or set) specified: %s' % use_switch)

    edges = defaultdict(dict)

    phenotype_tag = set()

    for paper in papers:
        for mutant in paper.mutants:

            if(conversion_dict != None):
                temp_list = []
                for x in mutant.mutations:
                    if(x.name.upper() in conversion_dict):
                        temp_list.extend(conversion_dict[x.name.upper()])
                gene_names = set(temp_list)
            else:
                gene_names = set([x.name for x in mutant.mutations])

            if(use_switch == 'tolerant'):
                phenotypes = mutant.resistant_phenotypes
            elif(use_switch == 'sensitive'):
                phenotypes = mutant.sensitive_phenotypes
                phenotypes.extend(mutant.antagonistic_phenotypes)
            elif(use_switch == 'all'):
                phenotypes = mutant.resistant_phenotypes
                phenotypes.extend(mutant.sensitive_phenotypes)
            else:
                raise AssertionError('Missing base case in bipartite function')

            #phenotypes.extend(mutant.antagonistic_phenotypes)

            for phenotype in phenotypes:

                tags = stress_ontology[phenotype.upper()]

                for tag in tags:
                    pairwise_interactions = [(tag, x) for x in gene_names]
                    for (t,g) in pairwise_interactions:
                        if(g in edges[t]):
                            edges[t][g]+=1
                        else:
                            edges[t][g] =1

    edge_tuples = []

    for gene_x in edges:
        for gene_y in edges[gene_x]:

            if(edges[gene_x][gene_y] > minimum_weight):
                phenotype_tag.add(gene_x)
                edge_tuples.append((gene_x, gene_y, {'weight': edges[gene_x][gene_y]}))

    G = networkx.Graph()
    G.add_edges_from(edge_tuples)

    for tag in phenotype_tag:
        G.node[tag]['color'] = 'b'
        G.node[tag]['size'] = 400

    return G

if(__name__ == '__main__'):

    #mainly testing code.

    import os
    import GraphUtils as glib
    import networkx

    standard_obj = Standard(os.path.join(DatabaseUtils.INPUT_DIR, 'Gene Name Mapping.txt'))

    phenotype_obj = Standard(os.path.join(DatabaseUtils.INPUT_DIR, 'Compound Information.txt'))

    stress_ontology, classifier = DatabaseUtils.load_tolerance_ontology()

    papers = DatabaseUtils.getDatabase(standard_obj,phenotype_obj)

    ecoli_cog_data = gene_ontology.load_eggnog_functional_annotations(os.path.join(DatabaseUtils.INPUT_DIR,'ecoli_cogs.txt'),standard_obj,'Escherichia coli')

    ecoli_go_data  = gene_ontology.load_species_dataset(os.path.join(DatabaseUtils.INPUT_DIR,'gene_association.ecocyc'),standard_obj,'Escherichia coli')

    ecoli_seed_data = DatabaseUtils.load_seed_subsystems(standard_obj,'Escherichia coli')

    #G = generate_genetic_interaction_network(papers, minimum_weight = 2)

    #glib.plot_network(G, 'Test Genetic Interaction Network', 'test_genetic_interaction.pdf', default_size=120)

    G = generate_bipartite_gene_phenotype_network(papers, stress_ontology, minimum_weight=2, conversion_dict=ecoli_seed_data)

    glib.plot_network(G, 'Test SEED Interaction Network', 'test_seed_interaction.pdf', default_size=120)

    networkx.write_pajek(G,'pajek.net')

    #G = generate_bipartite_gene_phenotype_network(papers, stress_ontology, minimum_weight=2)

    #glib.plot_network(G, 'Test Bipartite Interaction Network', 'test_bipartite.pdf', default_size=120, use_node_weights=True)


