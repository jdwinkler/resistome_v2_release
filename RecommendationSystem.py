__author__ = 'jdwinkler'

from collections import defaultdict
import DatabaseUtils
import itertools
import numpy
import gene_ontology
import numpy.random as random

import GraphUtils as glib

class Feature(object):

    #encapsulates

    def __init__(self, feature_name, feature_details):

        self.name = feature_name.upper()
        self.details = feature_details

    def __eq__(self, other):

        if(not isinstance(other, Feature)):
            return False
        else:
            return self.name == other.name and self.details == other.details

    def __hash__(self):

        return hash(self.name)

    def __str__(self):

        return self.name

class Vector:

    #feature name converter is standardized gene name to a list of features of some sort (string to list map)
    #examples: seed ontology, go ontology

    def __init__(self, internal_id, raw_features):

        self.id = internal_id
        self.feature_set = self.build_internal_features(raw_features)

    def build_internal_features(self, feature_set):

        output_set = set()
        output_set.update(feature_set)

        return output_set

    def integer_vector(self, feature_to_int, maximum):

        backing = [0] * maximum

        for f in self.feature_set:
            backing[feature_to_int[f.name]] = 1

        return backing

    def __len__(self):

        return len(self.feature_set)

    def combine(self, v2):

        return Vector(self.id + v2.id, self.feature_set.union(v2.feature_set))

    def union(self, v2):

        return len(self.feature_set.union(v2.feature_set))

    def intersection(self, v2):

        return len(self.feature_set.intersection(v2.feature_set))

    def difference(self, v2):

        return len(self.feature_set.difference(v2.feature_set))

    def depth_comparison(self, v2, network, lca_cache):

        product = set()
        for combo in itertools.product(self.feature_set, v2.feature_set):
            product.add(combo)

        score = 0.0

        seen_before = set()

        for (node_x, node_y) in product:

            if((node_x,node_y) in seen_before):
                continue

            if(node_y in lca_cache[node_x]):

                if(lca_cache[node_x][node_y] == -1):

                    if(len(v2.feature_set) == 1):
                        return -1, lca_cache
                    else:
                        continue
                else:

                    score+=lca_cache[node_x][node_y]
                    continue

            level = gene_ontology.ancestor_search(network, node_x.name,node_y.name)

            lca_cache[node_x][node_y] = level
            lca_cache[node_y][node_x] = level

            seen_before.add((node_x,node_y))
            seen_before.add((node_y,node_x))

        #basically, this looks through v2 to find the most closely related feature.
        for node in self.feature_set:

            #normalize by shortest

            try:
                score += min([lca_cache[node][x] for x in v2.feature_set if lca_cache[node][x] > -1])
            except:

                if(len(v2.feature_set) == 1):
                    return -1.0, lca_cache
                else:
                    continue

        return float(score)/float(len(self) * len(v2)), lca_cache

    def distance(self, v2, method = 'euclidean'):

        if(method == 'euclidean'):
            return numpy.sqrt(self.difference(v2))/float(len(self))

        if(method == 'cosine'):
            return 1-float(self.intersection(v2)) / (numpy.sqrt(len(self)) * numpy.sqrt(len(v2)))

        if(method == 'ochiai'):
            product = set()
            for combo in itertools.product(self.feature_set, v2.feature_set):
                product.add(combo)
            return 1-float(self.intersection(v2)) / float(len(product))

        if(method == 'jaccard'):
            return 1.0 - float(self.intersection(v2)) / (float(len(self) + len(v2)) - float(self.intersection(v2)))

        raise AssertionError('Requested method %s not yet implemented' % method)

#attempts to compute reliable statistics to identify (automatically)
#note that you can (will) have test vectors that incorporate features that are not included in the database, so it is probably
#best to transform that into something more standardized (GO, SEED-if they are still active, etc)
def compute_bootstrap_distance_vector(feature_vectors, feature_backing_set, design_features = 5, trials = 1000, feature_weights=None, method = 'euclidean', network_info = None):

    '''
    idea: make synthetic vector of fake features from the entire database, then compute pairwise distance scores for X such vectors

    '''

    if(method == 'network_depth' and network_info == None):
        raise AssertionError('You have to provide a (go_network, root node) tuple to use the network depth comparison method')

    #compute average number of features in each design
    average_design_features = numpy.mean([len(x) for x in feature_vectors])

    if(average_design_features > design_features):
        design_features = average_design_features

    distances = []

    weighted_sample = list(feature_backing_set)

    if(feature_weights != None):

        probabilities = [feature_weights.get(x, 0.0) for x in feature_backing_set]

    else:
        probabilities = []

    lca_cache = defaultdict(dict)

    for i in xrange(0, trials):

        #print i

        if(feature_weights == None):
            v = Vector('sample', random.choice(weighted_sample, size=design_features))
        else:
            v = Vector('sample', random.choice(weighted_sample, size=design_features, p=probabilities))

        #print [x.name for x in v.feature_set]

        for v2 in feature_vectors:

            if(method != 'network_depth'):
                distance = v.distance(v2, method=method)
                if(distance >= 0.0 and distance < 1.0):
                    distances.append(distance)
            else:
                distance, lca_cache = v.depth_comparison(v2, network_info, lca_cache)
                #print distance
                distances.append(distance)

    #glib.histogram(distances,'Distance of Random Models (method=%s,models=%i)' % (method, design_features),'Counts', '%s_test.pdf' % method, useMedianFilter=True)

    #compute 95% percentile of score for random models assembled from database features

    #print 'Limit for %s top scores (99th percentile, less than = above this threshold): %f' % (method, numpy.percentile(distances,1))

    return distances

def generate_vector_set(papers, type_of_feature, feature_converter = None, include_missing = True):

    if(feature_converter == None):
        feature_converter = {}

    feature_matrix = []

    all_features = set()

    for paper in papers:

        for mutant in paper.mutants:

            mutated_genes = mutant.affected_genes

            processed_feature_set = set()

            for gene in mutated_genes:
                if(gene.upper() in feature_converter):
                    processed_feature_set.update([Feature(x.upper(),type_of_feature) for x in feature_converter[gene.upper()]])
                elif(include_missing or feature_converter == {}):
                    processed_feature_set.add(Feature(gene.upper(),type_of_feature))

            v = Vector((paper.doi,mutant.id), processed_feature_set)

            if(len(v) > 0):
                all_features.update(processed_feature_set)
                feature_matrix.append(v)

    return feature_matrix, all_features

def find_score_percentile_cutoffs():

    print 'hi'

def find_related_models(proposed_vector, vector_set, method = 'euclidean', network_info = (None, None)):

    lca_cache = defaultdict(dict)

    distances = []

    for vector in vector_set:

        if(method != 'network_depth'):
            distance = proposed_vector.distance(vector, method=method)
        else:
            distance, lca_cache = proposed_vector.depth_comparison(vector, network_info[0], lca_cache)

        distances.append(distance)

    return distances

def pairwise_distance_matrix(vector_set, gene_ontology):

    pd = defaultdict(dict)

    seen_before = set()

    lca_cache = defaultdict(dict)

    counter = 0

    for v1 in vector_set:

        for v2 in vector_set:

            if(v1.id == v2.id or (v1.id,v2.id) in seen_before or (v2.id,v1.id) in seen_before):
                continue

            seen_before.add((v1.id, v2.id))
            seen_before.add((v2.id, v1.id))

            distance, lca_cache = v1.depth_comparison(v2, gene_ontology, lca_cache)

            #print distance

            pd[v1.id][v2.id] = distance
            pd[v2.id][v1.id] = distance

        counter+=1
        if(counter % 10 == 0):
            print counter, 'fraction finished: %f' % (float(counter)/float(len(vector_set)))

    return pd

if(__name__ == '__main__'):

    papers = DatabaseUtils.getDatabase()

    import Standardization
    import gene_ontology
    import os

    standard_obj = Standardization.Standard(os.path.join(DatabaseUtils.INPUT_DIR,'Gene Name Mapping.txt'))

    ecoli_ontology = gene_ontology.load_species_dataset(os.path.join(DatabaseUtils.INPUT_DIR,'gene_association.ecocyc'),standard_obj,'Escherichia coli')

    go_dict,master_go_network = gene_ontology.parse_GO_file(os.path.join(DatabaseUtils.INPUT_DIR,'go-basic.obo'),['is_a','name'])

    reduced_ontology = gene_ontology.most_specific_tags_only(ecoli_ontology, master_go_network,predecessor_level=0,minimum_root_distance=0)

    unique_tags = set()

    is_a_tags = set()

    for key in reduced_ontology:
        unique_tags.update(reduced_ontology[key])

        #print key, len(reduced_ontology[key])

    for tag in unique_tags:

        explanation = go_dict[tag]['is_a'].split(' ! ')[0]
        is_a_tags.add(explanation)

        #print tag, go_dict[tag]

    print 'Number of tags present in ontology: %i representing %i categories' % (len(unique_tags),len(is_a_tags))

    #seed_mapping = DatabaseUtils.load_seed_subsystems(standard_obj, 'Escherichia coli')

    vector_set, all_features = generate_vector_set(papers, 'ontology', include_missing=False, feature_converter=reduced_ontology)

    tag_weights = defaultdict(float)

    counter = defaultdict(int)

    for v in vector_set:
        for f in v.feature_set:
            counter[f]+=1

    total = float(sum(counter.values()))

    for tag in counter:
        tag_weights[tag] = float(counter[tag]) / total

    feature_size = 10
    trials = 100

    #distance_vector = compute_bootstrap_distance_vector(vector_set, all_features, trials=trials, design_features=feature_size, feature_weights = tag_weights, method='network_depth', network_info = (master_go_network, 'GO:0008150'))

    #distance_criteria = [1, 5, 10, 20]

    #for d in distance_criteria:

    #    print 'Limit for %s top scores (%ith percentile, less than = above this threshold): %f' % ('network_depth', d, numpy.percentile(distance_vector,d))
